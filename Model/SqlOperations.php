<?php

class SqlOperations{
    
    function sql_multiple_rows($sql){
        include "conn.php";
        $result = $conn->query($sql);
        $count = $result ? mysqli_num_rows($result) : -1;
        //mysqli_close($conn);
        unset($conn);
        return $count > 0 ? $result : $count;
    }
    
    function sql_single_row($sql){
        include "conn.php";
        $result = $conn->query($sql);
        $count = $result ? mysqli_num_rows($result) : -1;
        $row = '';
        if($count > 0)  
        {  
            $row = $result->fetch_assoc();
        }
        //mysqli_close($conn);
        unset($conn);
        return $row;
    }
    
    function sql_exec_op($sql){
        include "conn.php";
        $result = mysqli_query($conn, $sql);
        unset($conn);
        //mysqli_close($conn);
        return $result > 0 ? $result : 0;
    }
    
    function sql_exec_op_return($sql){
        include "conn.php";
        $conn->query("SET @si = ''");
        $conn->query($sql);
        $result = $conn->query("SELECT @si AS p_out");
        $row = $result->fetch_assoc();
        $output = $row['p_out'];
        unset($conn);
        //mysqli_close($conn);    
        return $output > 0 ? $output : -1;
    }
}
?>
