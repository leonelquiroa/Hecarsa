<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="description" content="Hecarsa Taller site">
        <meta name="author" content="@leoquiroa">
        <title>Vehiculos por Persona</title>
        <!--Third CSS-->
        <link href="../Controller/css/External/bootstrap.3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/dataTables.bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/bootstrap-select.1.12.0.min.css" rel="stylesheet" type="text/css"/>
        <!--Own CSS-->
        <link href="../Controller/css/part.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <!-- ################################################# MENU ################################################# -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="service.php">Hecarsa</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <!-- User -->
                <li>
                    <a href="#" data-toggle="collapse" data-target="#demoHome">
                        <i class="fa fa-object-ungroup"></i> Inicio <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoHome" class="collapse">
                        <li>
                            <a href="service.php"><i class="fa fa-home"></i> Servicio </a>
                            <a href="serviceLog.php"><i class="fa fa-list"></i> Historial </a>
                            <a href="calendar.php"><i class="fa fa-list"></i> Calendario </a>
                        </li>
                    </ul>
                </li>
                <li class="active">
                    <a href="#" data-toggle="collapse" data-target="#demoElements">
                        <i class="fa fa-object-group"></i> Elementos <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoElements" class="collapse">
                        <li>
                            <a href="part.php"><i class="fa fa-anchor"></i> Partes </a>
                            <a href="person.php"><i class="fa fa-users"></i> Personas </a>
                            <a href="VehiclePerPerson.php"><i class="fa fa-car"></i> Vehículos por Persona</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#demoConfigurations">
                        <i class="fa fa-cogs"></i> Configuraciones <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoConfigurations" class="collapse">
                        <li>
                            <a href="serviceType.php"><i class="fa fa-sellsy"></i> Tipos de Servicios </a>
                            <a href="PartPerServiceType.php"><i class="fa fa-xing-square"></i> Partes por Tipos de Servicio </a>
                            <a href="VehicleType.php"><i class="fa fa-angellist"></i> Tipos de Vehículos </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- ################################################# MENU ################################################# -->       
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">   
                <div style="font-size: 24px; text-align: center; color: #d9534f;">
                    <i class="fa fa-car"></i> VEHICULOS POR PERSONA
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div style="background-color: #5FC8FF; font-size: 16px;">
                            <img src="../Multimedia/Img/checklist_256.png" alt="" style="width: 25px; height: 25px;">
                            <strong>LISTADO DE VEHICULOS</strong>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div id="vehicle_person_div"></div>   
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div style="background-color: #5FC8FF; font-size: 16px;">
                            <img src="../Multimedia/Img/parts-512.png" alt="" style="width: 25px; height: 25px;">
                            <strong>DETALLE DEL TIPO DE VEHICULO</strong>
                        </div>
                        <br/>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-3">
                        <span  style="display: inline-block; height: 34px; ">Propietario</span><br/>
                        <span  style="display: inline-block; height: 34px; ">Tipo de Vehiculo</span><br/>
                        <span  style="display: inline-block; height: 34px; ">Placa</span><br/>
                        <span  style="display: inline-block; height: 34px; ">Marca</span><br/>
                        <span  style="display: inline-block; height: 34px; ">Linea</span><br/>
                        <span  style="display: inline-block; height: 34px; ">Color</span><br/>
                        <span  style="display: inline-block; height: 34px; ">Modelo</span><br/>
                    </div>
                    <div class="col-md-3">
                        <div id="catalogUnitTime"></div>
                        <div id="catalogOdometer"></div>
                        <input type="text" class="form-control EditPlate" id="txt_plate" placeholder="Número de placa">
                        <input type="text" class="form-control EditBrand" id="txt_brand" placeholder="Marca">
                        <input type="text" class="form-control EditLine" id="txt_line" placeholder="Linea">
                        <input type="text" class="form-control EditColor" id="txt_color" placeholder="Color">
                        <input type="text" class="form-control EditYear" id="txt_year" placeholder="Modelo">
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div style="background-color: #5FC8FF; font-size: 16px;">
                            <img src="../Multimedia/Img/parts-512.png" alt="" style="width: 25px; height: 25px;">
                            <strong>HOJA DE VIDA DEL VEHICULO</strong>
                        </div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <div style="background-color: #5FC8FF; font-size: 16px;">
                            <img src="../Multimedia/Img/parts-512.png" alt="" style="width: 25px; height: 25px;">
                            <strong>PARTES DISPONIBLES</strong>
                        </div>
                        <br/>
                        <div id="part_list_NOT_div"></div>
                    </div>
                    <div class="col-md-5">
                        <div style="background-color: #5FC8FF; font-size: 16px;">
                            <img src="../Multimedia/Img/parts-512.png" alt="" style="width: 25px; height: 25px;">
                            <strong>PARTES REGISTRADAS</strong>
                        </div>
                        <br/>
                        <div id="part_list_IN_div"></div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <hr/>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-4">
                                <button class="btn btn-block btn-success" type="button" id="btn_save">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-block btn-danger" type="button" id="btn_cancel">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </button>
                            </div>
                            <div class="col-md-4">
                                <button class="btn btn-block btn-warning" type="button" id="btn_delete">
                                    <i class="fa fa-trash" aria-hidden="true"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>
                <br/>
                <div id="dummy_div"></div>
            </div>  
        </div>  
    </div>
    <!--Third JS-->
    <script src="../Controller/js/External/jquery.3.1.0.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/bootstrap.3.3.7.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/jquery.dataTables.1.10.12.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/dataTables.bootstrap.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/dataTables.responsive.js" type="text/javascript"></script>
    <script src="../Controller/js/External/bootstrap-select.1.12.0.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/notify.min.js" type="text/javascript"></script>
    <!--Own JS-->
    <script src="../Controller/js/vehicleperperson.js" type="text/javascript"></script>
    </body>
</html>                    