<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0">
        <meta name="description" content="Hecarsa Taller site">
        <meta name="author" content="@leoquiroa">
        <title>Servicios por Persona</title>
        <!--Third CSS-->
        <link href="../Controller/css/External/bootstrap.3.3.7.min.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/sb-admin.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/font-awesome.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/bootstrap-select.1.12.0.min.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/External/bootstrap-datepicker.min.1.7.0.css" rel="stylesheet" type="text/css"/>
        <!--Own CSS-->
        <link href="../Controller/css/External/autocomplete.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/service.css" rel="stylesheet" type="text/css"/>
        <link href="../Controller/css/verticalScroll.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
    <!-- ################################################# MENU ################################################# -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation"> 
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="service.php">Hecarsa</a>
        </div>
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
                <!-- User -->
                <li class="active">
                    <a href="#" data-toggle="collapse" data-target="#demoHome">
                        <i class="fa fa-object-ungroup"></i> Inicio <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoHome" class="collapse">
                        <li>
                            <a href="service.php"><i class="fa fa-home"></i> Servicio </a>
                            <a href="serviceLog.php"><i class="fa fa-list"></i> Historial </a>
                            <a href="calendar.php"><i class="fa fa-list"></i> Calendario </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#demoElements">
                        <i class="fa fa-object-group"></i> Elementos <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoElements" class="collapse">
                        <li>
                            <a href="part.php"><i class="fa fa-anchor"></i> Partes </a>
                            <a href="person.php"><i class="fa fa-users"></i> Personas </a>
                            <a href="VehiclePerPerson.php"><i class="fa fa-car"></i> Vehículos por Persona</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#" data-toggle="collapse" data-target="#demoConfigurations">
                        <i class="fa fa-cogs"></i> Configuraciones <i class="fa fa-fw fa-caret-down"></i>
                    </a>
                    <ul id="demoConfigurations" class="collapse">
                        <li>
                            <a href="serviceType.php"><i class="fa fa-sellsy"></i> Tipos de Servicios </a>
                            <a href="PartPerServiceType.php"><i class="fa fa-xing-square"></i> Partes por Tipos de Servicio </a>
                            <a href="VehicleType.php"><i class="fa fa-angellist"></i> Tipos de Vehículos </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
    <!-- ################################################# MENU ################################################# -->       
    <div id="wrapper">
        <div id="page-wrapper">
            <div class="container-fluid">   
                <div style="font-size: 24px; text-align: center; color: #d9534f;">
                    <i class="fa fa-key"></i> SERVICIOS
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <input type="text" class="form-control" id="searchByName" style="width: 100%" placeholder="Nombre de la persona">
                    </div>
                    <div class="col-md-1" style="text-align: center;">ó</div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" id="searchByPlate" style="width: 100%" placeholder="Número de placa">
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <br/>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div id="person_head_div"></div>
                        <div id="vehicle_table_div"></div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div id="vehicle_head_div"></div>
                        <div id="old_service_table_div"></div>
                        <br>
                        <div class="row">
                            <div class="col-md-6">
                                <div id="ddl_servicetype_div"></div>
                            </div>
                            <div class="col-md-6"></div>
                        </div>
                        <br>
                        <div id="part_check_div"></div>
                        <br>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <div id="part_newService_div"></div>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-10">
                        <p style="font-size: 30px" id="nameService"></p>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div class="row">
                    <div class="col-md-1"></div>
                    <div class="col-md-3">
                        <span  style="display: inline-block; height: 34px;" id="lbl_currentOdometer">Odometro Actual</span><br/>
                        <span  style="display: inline-block; height: 34px;" id="lbl_nextOdometer">Odometro Siguiente</span><br/>
                        <span  style="display: inline-block; height: 34px;" id="lbl_currentDate">Fecha Servicio</span><br/>
                        <span  style="display: inline-block; height: 34px;" id="lbl_nextDate">Siguiente Servicio</span><br/>
                        <span  style="display: inline-block; height: 34px;" id="lbl_Note">Notas</span><br/><br/><br/><br/>
                        <span  style="display: inline-block; height: 34px;" id="lbl_Alert">Alertas</span><br/>
                    </div>
                    <div class="col-md-3">
                        <div class="row">
                            <div class="col-md-8">
                                <input type="text" class="form-control newCurrentOdometer" id="txt_currentOdometer" placeholder="Odometro Actual">
                            </div>
                            <div class="col-md-4">
                                <div id="ddl_odometer_div"></div>
                            </div>
                        </div>
                        <input type="text" class="form-control" id="txt_nextOdometer" placeholder="Odometro Siguiente" disabled="disabled">
                        <input type="text" class="form-control" id="txt_currentDate" placeholder="Fecha Servicio" >
                        <input type="text" class="form-control" id="txt_nextDate" placeholder="Siguiente Servicio" >
                        <textarea class="form-control" id="txt_Note" placeholder="Notas" rows="4"></textarea>
                        <textarea class="form-control" id="txt_Alert" placeholder="Alertas" rows="4"></textarea>
                    </div>
                    <div class="col-md-4">
                        <div id="summary_service_div"></div>
                        <p style="font-size: 40px" id="bigTotal"></p>
                        <br>
                        <button type="button" class="btn btn-success" id="btnSaveService" style="width: 100%;">Guardar</button>
                    </div>
                    <div class="col-md-1"></div>
                </div>
                <div id="dummy_div"></div>
            </div>
        </div>  
    </div>
    <!--Third JS-->
    <script src="../Controller/js/External/jquery.3.1.0.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/bootstrap.3.3.7.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/jquery-ui.1.12.1.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/bootstrap-select.1.12.0.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/notify.min.js" type="text/javascript"></script>
    <script src="../Controller/js/External/bootstrap-datepicker.min.1.7.0.js" type="text/javascript"></script>
    <!--Own JS-->
    <script src="../Controller/js/service.js" type="text/javascript"></script>
    </body>
</html>                    