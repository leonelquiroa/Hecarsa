var path = "../Controller/php/OpsCalendar.php";
var hoy = '';
function ajax_call(sent_url,sent_data,div,async_){
    $.ajax({
        cache: false,
        async: async_,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function calendar_call(sent_url,sent_data){
    $.ajax({
        cache: true,
        async: true,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){
            var obj = JSON.parse(d);
            $("#CalendarEvents").zabuto_calendar({
                legend: [
                    {type: "block", label: "Servicio", classname:'calendar_service'},
                    {type: "block", label: "Cumpleaños", classname:'calendar_birthday'},
                    {type: "block", label: "Mas de uno", classname:'calendar_both'}
                ],
                language: "es",
                today: true,
                cell_border: true,
                action: function() { myDateFunction(this.id); },
                data: obj,
                weekstartson: 0
            });
        }  
    });
}
function myDateFunction(id){
    var res = id.substring(id.length - 10);
    ajax_call(path,{type:'Details',fecha:res},"Details_div",true);
}
function init(){ 
    calendar_call(path,{type:'Calendar'});
    calculateHoy();
    ajax_call(path,{type:'Details',fecha:hoy},"Details_div",true);
}
function calculateHoy(){
    var fecha = new Date();
    var dd = fecha.getDate();
    var mm = (fecha.getMonth()+1);
    var yyyy = fecha.getFullYear();
    if(dd<10) { dd='0'+ dd; }
    if(mm<10) { mm='0'+ mm; }
    hoy = yyyy + '-' + mm + '-' + dd;
}

$(document).ready(function() {
    init();
});