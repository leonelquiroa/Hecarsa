var path = "../Controller/php/OpsPartPerServiceType.php";
var IdServiceType = -1;
var aliens = true;

function ajax_call(sent_url,sent_data,div){
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init(){ 
    ajax_call(path,{type:'table_service_type'},"table_service_type_div");
    resetDdl('Tipo de Servicio');
}
function resetDdl(title){
    ajax_call(path,{type:'category_ddl',title:title},"ddl_category_div");
    $('.selectpicker').selectpicker('show');
}
function refreshPartLists(){    
    ajax_call(path,{type:'dd_part_available', IdServiceType:IdServiceType},"dd_part_list_div");
    ajax_call(path,{type:'dd_part_assigned', IdServiceType:IdServiceType},"dd_part_assigned_div");
    ajax_call(path,{type:'table_assigned', IdServiceType:IdServiceType},"table_assigned_div");
    activateDragAndDrop();
}
function activateDragAndDrop(){
    $('#example-2-1 .sortable-list').sortable({
        connectWith: '#example-2-1 .sortable-list',
        update: function(event, ui) {
            if(aliens){
                var a = getItems('#example-2-1');
                var elements = a.split("|");
                if(elements != ""){
                    ajax_call(path,{type:'PartsOperations',IdServiceType:IdServiceType,idParts:elements[1]},"dummy_div");
                    refreshPartLists();
                    message("Se ha asociado la parte al servicio.", "success");
                }
                aliens=false;
            }
            else
                aliens=true;
        }
    });
}
function getItems(exampleNr){
    var columns = [];
    $(exampleNr + ' ul.sortable-list').each(function(){
        columns.push($(this).sortable('toArray').join(','));			
    });
    return columns.join('|');
}
function message(message, iconType)
{
    $.notify(message,iconType,
    { 
        position:"bottom center",
        clickToHide: true
    });
}

$(document).ready(function() {
    init();
});
$(document).on('changed.bs.select', '.selectpicker', function(){
    var nameDdl = this.id;
    var selected = $(this).find("option:selected").val();
    if(nameDdl === 'ddl_part_category'){
        IdServiceType = selected;
        refreshPartLists();
    }
});  