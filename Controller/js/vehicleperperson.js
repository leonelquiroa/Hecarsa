var path = "../Controller/php/OpsVehiclePerPerson.php";
var idvehicleperclient = -1;
var editable = false;
var idPerson = -1;
var idVehicleType = -1;
var gbl_Parts = [];
//------------------------------------------------------------------------------
function ajax_call(sent_url,sent_data,div)
{
    $.ajax({
        cache: false,
        async: false,
        url:sent_url,  
        method:"POST",
        data:sent_data,  
        dataType:"text",  
        success:function(d){  
            $('#'+div).html(d);
        }  
   });
}
function init()
{
    resetTables();
    resetDdls();
}
function resetTables()
{
    ajax_call(path,{type:'list_vehicle_person'},"vehicle_person_div");
    $('#dataTableVehicle').DataTable({
        responsive: true
    });
    refreshMemoryTables();
    //partListNOT(-1);
    //partListIN(-1);
}
function partListNOT(idVehicle){
    ajax_call(path,{type:'list_part_NOT',idVehicle:idVehicle},"part_list_NOT_div");
    $('#dataTablePartNOT').DataTable({
        responsive: true
    });
}
function partListIN(idVehicle){
    ajax_call(path,{type:'list_part_IN',idVehicle:idVehicle},"part_list_IN_div");
    $('#dataTablePartIN').DataTable({
        responsive: true
    });
}
function clean()
{
    $('#txt_plate').val('');
    $('#txt_brand').val('');
    $('#txt_line').val('');
    $('#txt_color').val('');
    $('#txt_year').val('');
    idPerson = -1;
    idVehicleType = -1;
    resetDdls();
    resetTables();
}
function resetDdls(){
    ajax_call(path,{type:'list_person',title:'Propietario'},"catalogUnitTime");
    ajax_call(path,{type:'list_vehicle_type',title:'Tipo de Vehiculo'},"catalogOdometer");
    $('.selectpicker').selectpicker('show');
}
function disableEnter(e)
{
    if(e.keyCode == 13)
    {
        e.preventDefault();   
    }
}
function editColumn(e, idObj, newValue, colName, txtFieldName, method)
{
    if(e.keyCode == 13)
    {          
       edit_data(idObj, newValue, colName, txtFieldName, method);
    }
}
function edit_data(idObj, texto, column_name, txtFieldName, method)  
{   
    $.post( path, { 
        type: method, 
        idObj: idObj, 
        column_name:column_name, 
        texto:texto
    }).done(function(data) 
    {
        if(parseInt(data) > 0){
            highlightRow("#8dc70a", txtFieldName);
            resetTables();
        }else{
            highlightRow("#d64b2f", txtFieldName);}
    });
}
function highlightRow(bgColor, txtFieldName)
{
    var rowSelector = $("#" + txtFieldName);
    rowSelector.css("background-color", bgColor);
    rowSelector.fadeTo("normal", 0.5, function() { 
        rowSelector.fadeTo("fast", 1, function() { 
            rowSelector.css("background-color", '');
        });
    });
}
function message(message, iconType)
{
    $.notify(message,iconType,
    { 
        position:"bottom center",
        clickToHide: true
    });
}
function setPartArray(idPart,nombre,imagen,code,brand){
    var obj = {
            'idParte' : idPart,
            'nombreParte': nombre,
            'imagenParte': imagen,
            'Codigo' : code,
            'Marca' : brand
        };
    gbl_Parts.push(obj); 
}
function getSavedObjs(array_){
    var savedParts = [];
    for (var j = 0;j < array_.length; j++) {
        savedParts.push(array_[j]["idParte"]);
    }
    return savedParts;
}
function findInObj(array, attr, value) {
    for(var i = 0; i < array.length; i += 1) {
        if(array[i][attr].toString() === value.toString()) {
            return i;
        }
    }
    return -1;
}
function refreshMemoryTables(){
    var partCheck = getSavedObjs(gbl_Parts);
    ajax_call(path,{
        type:'memory_part_IN'
        ,memoryParts:gbl_Parts
    },"part_list_IN_div",true);
    ajax_call(path,{
        type:'memory_part_NOT'
        ,checkList:partCheck.toString()
    },"part_list_NOT_div",true);
}
//------------------------------------------------------------------------------
$(document).ready(function() {
    init();
});
//show - done
$(document).on('click', '.SeeVehiclePerClient', function(){
    idvehicleperclient = $(this).data("id0");
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'show_vehicle_client',
            idvehicleperclient:idvehicleperclient
        },  
        dataType:"text",  
        success:function(d){
            if(d == '')
                message("Hubo un problema al recuperar la información de este vehiculo.", "error");
            else{
                var obj = JSON.parse(d);
                ajax_call(path,{type:'list_person',title:obj[0]},"catalogUnitTime");
                ajax_call(path,{type:'list_vehicle_type',title:obj[1]},"catalogOdometer");
                $('.selectpicker').selectpicker('show');
                $('#txt_plate').val(obj[2]);
                $('#txt_brand').val(obj[3]);
                $('#txt_line').val(obj[4]);
                $('#txt_color').val(obj[5]);
                $('#txt_year').val(obj[6]);
                editable = true;
                partListNOT(idvehicleperclient);
                partListIN(idvehicleperclient);
            }
        }  
    });
});
//delete - done
$(document).on('click', '#btn_delete', function(){
    $.ajax({
        cache: false,
        async: false,
        url:path,  
        method:"POST",
        data:{
            type:'remove_vehicle_client',
            idvehicleperclient:idvehicleperclient
        },  
        dataType:"text",  
        success:function(d){
            if(idvehicleperclient > 0 && d > 0 ){
                message("El vehiculo se ha eliminado correctamente.", "success");
                resetTables();
                resetDdls();
                clean();
                idvehicleperclient = -1;
            }else
                message("Hubo un problema al eliminar el vehiculo.", "error");
        }  
    });
});
//update
$(document).on('changed.bs.select', '.selectpicker', function(){
    var nameDdl = this.id;
    var selected = $(this).find("option:selected").val();
    var column_name = '';
    if(nameDdl === 'ddl_person')
    {
        column_name = 'idPerson';
        idPerson = selected;
        
    }
    else if(nameDdl === 'ddl_vehicle_type')
    {
        column_name = 'idVehicleType';
        idVehicleType = selected;
    }
    //
    if(editable){
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{ 
                type:'update_vehicle_client',
                id:idvehicleperclient,
                column_name:column_name,
                texto:selected
            },dataType:"text",
            success:function(Id){
                if(Id > 0){
                    message("El vehiculo por cliente " + column_name + " se ha actualizado correctamente.", "success");
                    resetTables();
                }else{
                    message("Hubo un problema al actualizar el vehiculo por cliente " + column_name + ".", "error");
                }
            }
        });
    }
});

$(document).on('keydown', '.EditPlate', function(e){
    disableEnter(e);
});
$(document).on('keydown', '.EditBrand', function(e){
    disableEnter(e);
});
$(document).on('keydown', '.EditLine', function(e){
    disableEnter(e);
});
$(document).on('keydown', '.EditColor', function(e){
    disableEnter(e);
});
$(document).on('keydown', '.EditYear', function(e){
    disableEnter(e);
});
$(document).on('keyup', '.EditPlate', function(e){  
    editColumn(e, 
        idvehicleperclient, 
        $("#txt_plate").val(), 
        "plateNumber",
        "txt_plate",
        "update_vehicle_client");
});
$(document).on('keyup', '.EditBrand', function(e){  
    editColumn(e, 
        idvehicleperclient, 
        $("#txt_brand").val(), 
        "brand",
        "txt_brand",
        "update_vehicle_client");
});
$(document).on('keyup', '.EditLine', function(e){  
    editColumn(e, 
        idvehicleperclient, 
        $("#txt_line").val(),
        "line",
        "txt_line",
        "update_vehicle_client");
});
$(document).on('keyup', '.EditColor', function(e){  
    editColumn(e, 
        idvehicleperclient, 
        $("#txt_color").val(), 
        "color",
        "txt_color",
        "update_vehicle_client");
});
$(document).on('keyup', '.EditYear', function(e){  
    editColumn(e, 
        idvehicleperclient, 
        $("#txt_year").val(), 
        "yearVehicle",
        "txt_year",
        "update_vehicle_client");
});
//cancel
$(document).on('click', '#btn_cancel', function(){
    clean();
    editable = false;
    idvehicleperclient = -1;
});
//save - done
$(document).on('click', '#btn_save', function(){
    var plate = $('#txt_plate').val();
    var brand = $('#txt_brand').val();
    var line = $('#txt_line').val();
    var color = $('#txt_color').val();
    var year = $('#txt_year').val();

    if(idPerson < 0) { message("Debe seleccionar una persona propietaria.", "info"); return;}
    if(idVehicleType < 0) { message("Debe seleccionar un tipo de vehiculo.", "info"); return;}
    if(plate === '') {message("Debe ingresar un número de placa.", "info"); return;}
    if(brand === '') {message("Debe ingresar una marca.", "info"); return;}
    if(line === '') {message("Debe ingresar la linea.", "info"); return;}
    if(color === '') {message("Debe ingresar el color.", "info"); return;}
    if(year === '') {message("Debe ingresar el modelo.", "info"); return;}

    if(idvehicleperclient < 0)
    {
        saveOldParts();
        
        $.ajax({
            cache: false,
            async: false,
            url:path,  
            method:"POST",
            data:{ 
                type:'add_vehicle_client',  
                idPerson:idPerson,
                idVehicleType:idVehicleType,
                plate:plate,
                brand:brand,
                line:line,
                color:color,
                year:year
            },  
            dataType:"text",  
            success:function(newId){
                if(newId > 0){
                    saveParts(newId);
                    message("El vehiculo se ha guardado correctamente.", "success");
                    clean();
                    resetTables();
                    resetDdls();
                    var r = confirm("¿Desea agregar un nuevo servicio al vehículo?");
                    if (r === true){ window.location="service.php"; }
                }else{
                    message("Hubo un problema al guardar el vehiculo.", "error");
                }
            }
        });
    }
});
function saveParts(idVehicle){
    for(var i = 0; i < gbl_Parts.length; i += 1) {
        ajax_call(path,{
            type:'add_part'
            ,idVehicle:idVehicle
            ,idPart:gbl_Parts[i]["idParte"]
            ,codePart:gbl_Parts[i]["Codigo"]
            ,brandPart:gbl_Parts[i]["Marca"]
        },"dummy_div");
    }
}
//see part list
$(document).on('click', '.PartNameNOT', function(){
    var idPart = $(this).data("id0");
    if(idvehicleperclient > 0){ //existing car
        ajax_call(path,{
            type:'add_part'
            ,idVehicle:idvehicleperclient
            ,idPart:idPart
            ,codePart:''
            ,brandPart:''
        },"dummy_div");
        partListNOT(idvehicleperclient);
        partListIN(idvehicleperclient);
    }else{  //new car
        saveOldParts();
        setPartArray(
            idPart
            ,$(this).text()
            ,$(this).data("id1")
            ,'','');
        refreshMemoryTables();
    }
});
$(document).on('click', '.PartNameIN', function(){
    if(idvehicleperclient > 0){ //existing car
        var idResume = $(this).data("id0");
        ajax_call(path,{
            type:'delete_part'
            ,idResume:idResume
        },"dummy_div");
        partListNOT(idvehicleperclient);
        partListIN(idvehicleperclient);
    }else{  //new car
        saveOldParts();
        var ixP = findInObj(gbl_Parts, "idParte", $(this).data("id0"));
        gbl_Parts.splice(ixP, 1);
        refreshMemoryTables();
    }
});
function saveOldParts(){
    var jsTable = document.getElementById("dataTablePartIN");
    if(jsTable !== null){
        var NoRows = jsTable.rows.length;
        for (var i = 1; i < NoRows; i++) {
            var fila = jsTable.rows[i];
            var lcl_id = $(fila.cells[0]).data("id0");
            var ixP = findInObj(gbl_Parts, "idParte", lcl_id);
            gbl_Parts[ixP]["Codigo"] = fila.cells[2].innerHTML.replace(/\<br\>/g, "");
            gbl_Parts[ixP]["Marca"] = fila.cells[3].innerHTML.replace(/\<br\>/g, "");
        }
    }
}
//edit part
$(document).on('keydown', '.PartCode', function(e){
    disableEnter(e);
});
$(document).on('keydown', '.PartBrand', function(e){
    disableEnter(e);
});
$(document).on('keyup', '.PartCode', function(e){  
    var idResume = $(this).data("id0");
    editColumn(e
        ,idResume
        ,$(this).text()
        ,"codePart"
        ,"PartCode"+idResume
        ,"update_part");
});
$(document).on('keyup', '.PartBrand', function(e){  
    var idResume = $(this).data("id0");
    editColumn(e
        ,idResume
        ,$(this).text()
        ,"brandPart"
        ,"PartBrand"+idResume
        ,"update_part");
});
