<?php
    include "../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();      
    include "../../Controller/php/general.php";
    $fns = new generalFunctions();
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    switch ($type_data)
    {
        //vehicle list
        case 'list_vehicle_person':
            $result = $sqlOps->sql_multiple_rows("CALL vehicleperclient_list()");
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $list = '';
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <tr>
                            <td style="cursor:pointer;" data-id0="'.$row["idvehicleperclient"].'" class="SeeVehiclePerClient">'.$row["namePerson"].'</td>
                            <td>'.$row["nameVehicleType"].'</td>
                            <td>'.$row["PlateNumber"].'</td>
                            <td>'.$row["Brand"].'</td>
                            <td>'.$row["Line"].'</td>
                            <td>'.$row["Color"].'</td>
                            <td>'.$row["YearVehicle"].'</td>
                        </tr>';
                }
                $output .= '
                <table width="100%" class="table table-condensed table-bordered table-hover" id="dataTableVehicle" style="font-size: 12px; text-align:center;">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Propietario</th>
                            <th style="text-align:center;">Tipo de Vehiculo</th>
                            <th style="text-align:center;">Placa</th>        
                            <th style="text-align:center;">Marca</th>        
                            <th style="text-align:center;">Linea</th>        
                            <th style="text-align:center;">Color</th>        
                            <th style="text-align:center;">Modelo</th>        
                        </tr>
                    </thead>
                    <tbody>';
                $output .= $list;
                $output .= '    
                        </tbody>
                    </table>';
            }
        break;
        //retrieve vehicle info
        case 'show_vehicle_client':
            $sql = "CALL vehicleperclient_get(".$_POST['idvehicleperclient'].")";
            $row = $sqlOps->sql_single_row($sql);
            if($row == ''){
                $output = '';
            }else{
                $res = array();
                $res[0] = $row["namePerson"];
                $res[1] = $row["nameVehicleType"];
                $res[2] = $row["PlateNumber"];
                $res[3] = $row["Brand"];
                $res[4] = $row["Line"];
                $res[5] = $row["Color"];
                $res[6] = $row["YearVehicle"];
                echo json_encode($res);
            }
        break;
        //ddl for vehicle (owner, vehicle type)
        case 'list_person':
            $output = $fns->getListFullWithSearch(
                $sqlOps, 
                "CALL person_list()", 
                "ddl_person", 
                $_POST['title'], 
                "idPerson", 
                "namePerson");
        break;
        case 'list_vehicle_type':
            $output = $fns->getListFull(
                $sqlOps, 
                "CALL catalog_list('VehicleType')", 
                "ddl_vehicle_type", 
                $_POST['title'], 
                "idCatalog", 
                "nameCatalog");
        break;
        //Vehicle operations
        case 'remove_vehicle_client':
            $sql = "CALL vehicleperclient_remove(".$_POST['idvehicleperclient'].")";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        case 'update_vehicle_client':
            $sql = "CALL vehicleperclient_update("
                . "'".$_POST['idObj']."'"
                . ",'".$_POST['column_name']."'"
                . ",'".$_POST['texto']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        case 'add_vehicle_client':
            $sql = "CALL vehicleperclient_add("
                . "'".$_POST['idPerson']."'"
                . ",'".$_POST['idVehicleType']."'"
                . ",'".$_POST['plate']."'"
                . ",'".$_POST['brand']."'"
                . ",'".$_POST['line']."'"
                . ",'".$_POST['color']."'"
                . ",'".$_POST['year']."'"
                . ",@si)";
            $output = $sqlOps->sql_exec_op_return($sql);    
        break;
        //part list
        case 'list_part_NOT':
            $result = $sqlOps->sql_multiple_rows("CALL resumevehicle_list_NOT(".$_POST['idVehicle'].")");
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $list = '';
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <tr>
                            <td class="PartNameNOT" style="vertical-align: middle; cursor:pointer;" data-id0="'.$row["idPart"].'">'.$row["namePart"].'</td>
                            <td><img src="../Multimedia/Parts/'.$row["imageUrl"].'" alt="" style="width: 25px; height: 25px;"></td>
                        </tr>';
                }
                $output .= '
                <table width="100%" class="table table-condensed table-bordered table-hover" id="dataTablePartNOT" style="font-size: 12px; text-align:center;">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Nombre</th>
                            <th style="text-align:center;">Imagen</th>        
                        </tr>
                    </thead>
                    <tbody>';
                $output .= $list;
                $output .= '    
                        </tbody>
                    </table>';
            }
        break;
        case 'list_part_IN':
            $result = $sqlOps->sql_multiple_rows("CALL resumevehicle_list_IN(".$_POST['idVehicle'].")");
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $list = '';
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <tr>
                            <td class="PartNameIN" style="vertical-align: middle; cursor:pointer;" data-id0="'.$row["idPart"].'" data-id1="'.$row["idResume"].'">'.$row["namePart"].'</td>
                            <td><img src="../Multimedia/Parts/'.$row["imageUrl"].'" alt="" style="width: 25px; height: 25px;"></td>
                            <td id="PartCode'.$row["idResume"].'" data-id0="'.$row["idResume"].'" class="PartCode" contenteditable="true">'.$row["codePart"].'</td>
                            <td id="PartBrand'.$row["idResume"].'" data-id0="'.$row["idResume"].'" class="PartBrand" contenteditable="true">'.$row["brandPart"].'</td>
                        </tr>';
                }
                $output .= '
                <table width="100%" class="table table-condensed table-bordered table-hover" id="dataTablePartIN" style="font-size: 12px; text-align:center;">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Nombre</th>
                            <th style="text-align:center;">Imagen</th>
                            <th style="text-align:center;">Código</th>
                            <th style="text-align:center;">Marca</th>
                        </tr>
                    </thead>
                    <tbody>';
                $output .= $list;
                $output .= '    
                        </tbody>
                    </table>';
            }else{
                $output .= '<<<<<<<<<<<<<< Sin partes registradas al vehículo >>>>>>>>>>>>>>>>>>>>';
            }
        break;
        case 'add_part':
            $sql = "CALL resumevehicle_add("
                . "'".$_POST['idVehicle']."'"
                . ",'".$_POST['idPart']."'"
                . ",'".$_POST['codePart']."'"
                . ",'".$_POST['brandPart']."'"
                . ",@si)";
            $output = $sqlOps->sql_exec_op_return($sql);            
        break;
        case 'delete_part':
            $sql = "CALL resumevehicle_delete(".$_POST['idResume'].")";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        case 'update_part':
            $sql = "CALL resumevehicle_update("
                . "'".$_POST['idObj']."'"
                . ",'".$_POST['column_name']."'"
                . ",'".$_POST['texto']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        //memory
        case 'memory_part_NOT':
            $checkList = $_POST['checkList'];
            if(count($checkList)>0){
                $sql = "CALL resumevehicle_memory('".$_POST['checkList']."','NOT')";
            }else{
                $sql = "CALL resumevehicle_memory(NULL,'ALL')";
            }
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $list = '';
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <tr>
                            <td class="PartNameNOT" style="vertical-align: middle; cursor:pointer;" data-id0="'.$row["idPart"].'" data-id1="'.$row["imageUrl"].'">'.$row["namePart"].'</td>
                            <td><img src="../Multimedia/Parts/'.$row["imageUrl"].'" alt="" style="width: 25px; height: 25px;"></td>
                        </tr>';
                }
                $output .= '
                <table width="100%" class="table table-condensed table-bordered table-hover" id="dataTablePartNOT" style="font-size: 12px; text-align:center;">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Nombre</th>
                            <th style="text-align:center;">Imagen</th>
                        </tr>
                    </thead>
                    <tbody>';
                $output .= $list;
                $output .= '    
                        </tbody>
                    </table>';
            }else{
                $output .= '<<<<<<<<<<<<<< Sin partes registradas al vehículo >>>>>>>>>>>>>>>>>>>>';
            }
        break;
        case 'memory_part_IN':
            $list = '';
            foreach ($_POST['memoryParts'] as &$rowMem){
                $list .= '
                <tr>
                    <td class="PartNameIN" style="vertical-align: middle; cursor:pointer;" data-id0="'.$rowMem["idParte"].'">'.$rowMem["nombreParte"].'</td>
                    <td><img src="../Multimedia/Parts/'.$rowMem["imagenParte"].'" alt="" style="width: 25px; height: 25px;"></td>
                    <td contenteditable="true">'.$rowMem['Codigo'].'</td>
                    <td contenteditable="true">'.$rowMem['Marca'].'</td>
                </tr>';
            }
            $output .= '
            <table width="100%" class="table table-condensed table-bordered table-hover" id="dataTablePartIN" style="font-size: 12px; text-align:center;">
                <thead>
                    <tr>
                        <th style="text-align:center;">Nombre</th>
                        <th style="text-align:center;">Imagen</th>
                        <th style="text-align:center;">Código</th>
                        <th style="text-align:center;">Marca</th>
                    </tr>
                </thead>
                <tbody>';
            $output .= $list;
            $output .= '    
                    </tbody>
                </table>';
        break;
        /*case '':
        break;*/
    }
    echo $output == '' ? '' : $output;