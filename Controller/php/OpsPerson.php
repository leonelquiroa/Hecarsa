<?php
    include "../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();      
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    switch ($type_data)
    {
        //initial show
        case 'list_person':
            $result = $sqlOps->sql_multiple_rows("CALL person_list()");
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $list = '';
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <tr>
                            <td class="PersonInfo" style="vertical-align: middle; cursor:pointer;" data-id0="'.$row["idPerson"].'">'.$row["legalName"].'</td>
                            <td class="PersonInfo" style="vertical-align: middle; cursor:pointer;" data-id0="'.$row["idPerson"].'">'.$row["namePerson"].'</td>
                            <td style="vertical-align: middle;">'.$row["NIT"].'</td>
                            <td style="vertical-align: middle;">'.$row["Phone"].'</td>
                            <td style="vertical-align: middle;">'.$row["Birthday"].'</td>
                            <td style="vertical-align: middle; cursor:pointer;">
                                <i class="fa fa-trash DeletePerson" aria-hidden="true" data-id1="'.$row["idPerson"].'"></i>
                            </td>
                        </tr>';
                }
                $output .= '
                <table width="100%" class="table table-condensed table-bordered table-hover" id="dataTables-example" style="font-size: 12px; text-align:center;">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Empresa</th>
                            <th style="text-align:center;">Nombre</th>
                            <th style="text-align:center;">NIT</th>        
                            <th style="text-align:center;">Telefono</th>
                            <th style="text-align:center;">Cumpleaños</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>';
                $output .= $list;
                $output .= '    
                        </tbody>
                    </table>';
            }
        break;
        //save
        case 'add_person':
            $sql = "CALL person_add("
                . "'".$_POST['legal']."',"
                . "'".$_POST['name']."',"
                . "'".$_POST['nit']."',"
                . "'".$_POST['phone']."',"
                . "'".$_POST['birth']."',"
                . "@si)";
            $output = $sqlOps->sql_exec_op_return($sql);            
        break;
        //get
        case 'show_person':
            $sql = "CALL person_get(".$_POST['idPerson'].")";
            $row = $sqlOps->sql_single_row($sql);
            if($row != ''){
                $res = array();
                $res[0][0] = $row["idPerson"];
                $res[0][1] = $row["legalName"];
                $res[0][2] = $row["namePerson"];
                $res[0][3] = $row["NIT"];
                $res[0][4] = $row["Phone"];
                $res[0][5] = $row["Birthday"];
                echo json_encode($res);
            }
        break;
        //update
        case 'update_person':
            $sql = "CALL person_update('".$_POST['id']."','".$_POST['column_name']."','".$_POST['texto']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        //delete
        case 'remove_person_db':
            $sql = "CALL person_delete(".$_POST['idPerson'].")";
            $output = $sqlOps->sql_exec_op($sql);
        break;
    }
    echo $output == '' ? '' : $output;
