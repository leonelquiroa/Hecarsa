<?php
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    include "../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();  
    
    include "../../Controller/php/general.php";
    $fns = new generalFunctions();   
    
    switch ($type_data){
        case 'Calendar':
            $sql = "CALL calendar_list(NULL,'General')";
            $res = $fns->get_array_calendar_event($sql,$sqlOps);
            echo json_encode($res);
        break;
        case 'Details':
            $sqlCheck = "CALL calendar_list('".$_POST['fecha']."','ForToday')";
            $resultCheck = $sqlOps->sql_single_row($sqlCheck);
            $output = $resultCheck["DiaNombre"]
                    .'<h1>'.$resultCheck["Dia"].'</h1>'
                    .$resultCheck["Mes"].', '.$resultCheck["Anio"]
                    .'<br/><br/>';
            
            if(intval($resultCheck["Data"])>0){
                $sql = "CALL calendar_list('".$_POST['fecha']."','Details')";
                $result = $sqlOps->sql_multiple_rows($sql);
                $count = $result ? mysqli_num_rows($result) : -1;
                $flag = true;
                if($count > 0){
                    $output .= '
                    <table class="table table-bordered table-condensed" style="text-align:center; font-size: 12px; ">
                        <thead>
                            <tr>
                                <th width="25%" style="text-align:center; color: #22CEDC;"><b>Tipo</b></th>
                                <th width="45%" style="text-align:center; color: #CCB05E;"><b>Nombre y Telefono</b></th>
                            </tr>
                        </thead>
                        <tbody>';
                    while($row = $result->fetch_assoc()){
                        //the n-events
                        $output .= '
                        <tr>
                            <td width="25%" style="vertical-align: middle;">'.$row["Tipo"].'</td>
                            <td width="45%" style="vertical-align: middle;"><b>'.$row["Persona"].'</b><br/>'.$row["Telefono"].'</td>
                        </tr>';
                    }
                    $output .= '
                        </tbody>
                    </table>';
                }else{
                    $output = "No hay servicios ni cumpleaños para hoy";
                }
            }else{
                $output .= "
                        <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
                        <br/>
                        Hoy no hay servicios ni cumpleaños
                        <br/>
                        >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>";
            }
        break;
    }
    echo $output == '' ? '' : $output;