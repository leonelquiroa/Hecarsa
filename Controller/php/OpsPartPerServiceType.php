<?php
    include "../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();    
    include "../../Controller/php/general.php";
    $fns = new generalFunctions();    
    
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    
    switch ($type_data)
    {
        //initial show
        case 'category_ddl':
            $output = $fns->getListFull(
                $sqlOps, 
                "CALL partperservice_list()", 
                "ddl_part_category", 
                $_POST['title'], 
                "idServiceType", 
                "nameServiceType");
        break;
        //after selecting a service type
        case 'dd_part_available':
            $sql = "CALL partperservice_notassigned_list(".$_POST['IdServiceType'].")";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0) {
                $list = '';
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <li class="sortable-item" id="'.$row["idPart"].'">
                            <img src="../Multimedia/Parts/'.$row["imageUrl"].'" alt="" style="width:70px;height:70px;"/>
                            <br/>
                            <p style="font-size:10px;">'.$row["namePart"].' - '.$row["categoryName"].'</p>
                        </li>';              
                }
                $output .= '<ul class="sortable-list" id="availableList">';
                $output .= $list;
                $output .= '</ul>';
            }
        break;
        case 'dd_part_assigned':
            $sql = "CALL partperservice_assigned_list(".$_POST['IdServiceType'].")";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '';
            if($count > 0) {
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <li class="sortable-item" id="'.$row["idPart"].'">
                            <img src="../Multimedia/Parts/'.$row["imageUrl"].'" alt="" style="width:70px;height:70px;"/>
                            <br/>
                            <p style="font-size:10px;">'.$row["namePart"].' - '.$row["categoryName"].'</p>
                        </li>';    
                }
            }
            $output .= '<ul class="sortable-list" id="assignedList">';
            $output .= $list;   
            $output .= '</ul>';
        break;
        case 'table_assigned':
            $sql = "CALL partperservice_assigned_list(".$_POST['IdServiceType'].")";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $list = '';
            if($count > 0) {
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <tr>
                            <td>'.$row["categoryName"].'</td>
                            <td>'.$row["namePart"].'</td>
                            <td>
                                <img src="../Multimedia/Parts/'.$row["imageUrl"].'" alt="" style="width:25px;height:25px;"/>
                            </td>
                        </tr>';    
                }    
            }
            $output .= '
                <table class="table table-condensed table-hover table-bordered scroll" style="font-size:12px; text-align:center;" id="tablePartAssigned">
                    <tbody>';
            $output .= $list;
            $output .= '
                    </tbody>
                    </table>';
        break;
        //ops
        case 'PartsOperations':
            $sql = "CALL partperservice_assigned_list(".$_POST['IdServiceType'].")";
            $result = $sqlOps->sql_multiple_rows($sql);
            $count = $result ? mysqli_num_rows($result) : -1;
            $listDb = array(); 
            $i = 0;
            if($count > 0) {
                while($row = $result->fetch_assoc()){
                    $listDb[$i++] = $row["idPart"];
                }
            }
            //
            $listWeb = split(",",$_POST['idParts']);
            //
            $ToAdd = array_diff($listWeb, $listDb);
            foreach ($ToAdd as $uniquePart) {
                if($uniquePart != ''){
                    $sql = "CALL partperservice_add(".$uniquePart.",".$_POST['IdServiceType'].")";
                    $sqlOps->sql_exec_op($sql);
                }
            }
            $ToRemove = array_diff($listDb,$listWeb);
            foreach ($ToRemove as $uniquePart) {
                if($uniquePart != ''){
                    $sql = "CALL partperservice_remove(".$uniquePart.",".$_POST['IdServiceType'].")";
                    $sqlOps->sql_exec_op($sql);
                }
            }
        break;
    }
    echo $output == '' ? '' : $output;
