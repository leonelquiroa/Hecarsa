<?php
include "../../Model/SqlOperations.php";
$sqlOps = new SqlOperations();    
include "../../Controller/php/general.php";
$fns = new generalFunctions();    

$output = '';
$type_data = isset($_POST['type']) ? $_POST['type'] : '';

switch ($type_data)
{
    //OLD
    case 'PersonHead':
        $result = $sqlOps->sql_single_row("CALL person_get(".$_POST['idPerson'].")");
        if($result != ''){
            $output = 
                "<p>"
                    . "Propietario: <span>".$result["legalName"]." / ".$result["namePerson"]."</span>"
                    . " - NIT: <span>".$result["NIT"]."</span>"
                    . " - Telefono: <span>".$result["Phone"]."</span>"
                    . " - Fecha de Nac.: <span>".$result["Birthday"]."</span>"
                . "</p>";
        }
    break;
    case 'VehiclesTable':
        $result = $sqlOps->sql_multiple_rows("CALL vehicleperclient_getByPerson(".$_POST['idPerson'].")");
        $count = $result ? mysqli_num_rows($result) : -1;
        if($count > 0){
            $list = '';
            while($row = $result->fetch_assoc()){
                $list .= '
                    <tr>
                        <td style="cursor:pointer;" class="vehicleList" data-id0="'.$row["idvehicleperclient"].'">'.$row["plateNumber"].'</td>
                        <td >'.$row["brand"].'</td>
                        <td >'.$row["color"].'</td>
                    </tr>';
            }
            $output .= '
            <table width="100%" class="table table-condensed table-bordered table-hover" style="font-size: 12px; text-align:center;">
                <thead>
                    <tr>
                        <th style="text-align:center;">Placa</th>
                        <th style="text-align:center;">Marca</th>
                        <th style="text-align:center;">Color</th>        
                    </tr>
                </thead>
                <tbody>';
            $output .= $list;
            $output .= '    
                    </tbody>
                </table>';
        }
    break;
    case 'VehiclesHead':
        $result = $sqlOps->sql_single_row("CALL vehicleperclient_get(".$_POST['idVehicle'].")");
        if($result != ''){
            $output = 
                "<p>"
                    . "Tipo: <span>".$result["nameVehicleType"]."</span>"
                    . " - Placa: <span>".$result["PlateNumber"]."</span>"
                    . " - Marca: <span>".$result["Brand"]."</span>"
                    . " - Linea: <span>".$result["Line"]."</span>"
                    . " - Color: <span>".$result["Color"]."</span>"
                    . " - Modelo: <span>".$result["YearVehicle"]."</span>"
                . "</p>";
        }
    break;
    case 'ServicesTable':
        $result = $sqlOps->sql_multiple_rows("CALL service_ByVehicle(".$_POST['idVehicle'].")");
        $count = $result ? mysqli_num_rows($result) : -1;
        if($count > 0){
            $list = '';
            while($row = $result->fetch_assoc()){
                $list .= '
                    <tr>
                        <td >'.$row["currentTimeService"].'</td>
                        <td >'.$row["nameServiceType"].'</td>
                        <td >'.$row["currentOdometer"].' '.$row["nameOdometerType"].'</td>
                        <td > Q. '.$row["cost"].'</td>
                        <td >'.$row["note"].'</td>
                    </tr>';
            }
            $output .= '
            <table width="100%" class="table table-condensed table-bordered table-hover" style="font-size: 12px; text-align:center;">
                <thead>
                    <tr>
                        <th style="text-align:center;">Fecha</th>
                        <th style="text-align:center;">Servicio</th>
                        <th style="text-align:center;">Odometro</th>        
                        <th style="text-align:center;">Costo</th>
                        <th style="text-align:center;">Notas</th>
                    </tr>
                </thead>
                <tbody>';
            $output .= $list;
            $output .= '    
                    </tbody>
                </table>';
        }else{
            $output .= '<<<<<<<<<<<<<<<<<< Sin registro de servicios >>>>>>>>>>>>>>>>>>>>>>>>>>';
        }
    break;
    //NEW
    case 'DdlServiceType':
        $output = $fns->getListFull(
            $sqlOps, 
            "CALL partperservice_list()", 
            "ddl_service_type", 
            $_POST['title'], 
            "idServiceType", 
            "nameServiceType");
    break;
    case 'PartPerServiceTableCheck':
        $result = $sqlOps->sql_multiple_rows("CALL partperservice_assigned_list(".$_POST['idNewService'].")");
        $count = $result ? mysqli_num_rows($result) : -1;
        if($count > 0){
            $list = '';$NoCheck = 0;
            $partList = isset($_POST['checkList']) ? $_POST['checkList'] : [];
            while($row = $result->fetch_assoc()){
                $NoCheck++;
                $status = in_array($row["idPart"], $partList) ? 'checked' : '';
                $list .= '
                    <tr>
                        <td >'.$row["namePart"].'</td>
                        <td ><input type="checkbox" class="classCheckPart" value="'.$row["idPart"].'" '.$status.'></td>
                    </tr>';
            }
            $status = $NoCheck == count($partList) ? 'checked' : '';
            $output .= '
            <table width="100%" class="table table-condensed table-bordered table-hover" style="font-size: 12px; text-align:center;">
                <thead>
                    <tr>
                        <th style="text-align:center;">Parte</th>
                        <th style="text-align:center;">
                            <input type="checkbox" id="mainCheck" class="classCheckPart" value="0" '.$status.'>
                        </th>
                    </tr>
                </thead>
                <tbody>';
            $output .= $list;
            $output .= '    
                    </tbody>
                </table>';
        }
        else{
            $output .= '<<<<<<<<<<<<<<<<<< Sin configuracion de partes por servicio >>>>>>>>>>>>>>>>>>>>>>>>>>';
        }
    break;
    case 'currentServiceTable':
        if(isset($_POST['memoryParts'])){
            $memoryParts = $_POST['memoryParts'];
            $list = '';
            foreach ($memoryParts as &$rowMem) {
                $resultDB = $sqlOps->sql_single_row("CALL part_service_list(".$rowMem['Servicio'].",'".$rowMem['Parte']."')");
                if($resultDB != ''){
                    $idRow = $rowMem['Servicio'].'%'.$rowMem['Parte'];
                    $list .= '
                        <tr>
                            <td >'.$resultDB["serviceName"].'</td>
                            <td >'.$resultDB["namePart"].'</td>
                            <td ><img src="../Multimedia/Parts/'.$resultDB["imageUrl"].'" height="30" width="30"></td>
                            <td contenteditable="true">'.$rowMem['Codigo'].'</td>
                            <td contenteditable="true">'.$rowMem['Marca'].'</td>
                            <td contenteditable="true" class="newPricePart" data-id0="'.$idRow.'" data-id0="chuchito">'.$rowMem['Precio'].'</td>
                            <td contenteditable="true">'.$rowMem['Nota'].'</td>
                        </tr>';
                }
            }
            $output .= '
                <table width="100%" class="table table-condensed table-bordered table-hover" style="font-size: 12px; text-align:center; width:100%" id="tableNewService">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Servicio</th>
                            <th style="text-align:center;">Parte</th>
                            <th style="text-align:center;">Imagen</th>
                            <th style="text-align:center;">Código</th>
                            <th style="text-align:center;">Marca</th>
                            <th style="text-align:center;">Precio Q.</th>
                            <th style="text-align:center;">Nota</th>
                        </tr>
                    </thead>
                    <tbody>';
                $output .= $list;
                $output .= '    
                    </tbody>
                </table>';
        }
    break;
    case 'summaryPrices':
        if(isset($_POST['memoryServices'])){
            $memoryServices = $_POST['memoryServices'];
            $list = '';
            foreach ($memoryServices as &$rowMem){
                $list .= '
                <tr>
                    <td style="cursor:pointer;" class="GeneralService" data-id0="'.$rowMem["idServicio"].'">'.$rowMem['nombreServicio'].'</td>
                    <td id="'.$rowMem['idServicio'].'">'.$rowMem['Costo'].'</td>
                </tr>';
            }
            $output .= '
                <table width="100%" class="table table-condensed table-bordered table-hover" style="font-size: 12px; text-align:center; width:100%" id="tableSummaryService">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Servicio</th>
                            <th style="text-align:center;">Costo</th>
                        </tr>
                    </thead>
                    <tbody>';
                $output .= $list;
                $output .= '    
                    </tbody>
                </table>';
        }
    break;
    case 'getServiceName': 
        $resultDB = $sqlOps->sql_single_row("CALL servicetype_get(".$_POST['idNewService'].")");
        if($resultDB != ''){
            $res = array();
            $res[0] = $resultDB["nameServiceType"];
            $res[1] = $resultDB["currentDateTime"];
            $res[2] = $fns->nextServiceDate($resultDB["currentDateTime"], $resultDB["timeServiceType"], $resultDB["unitTime"]);
            $res[3] = $fns->nextOdometerKm($resultDB["Odometer"],$resultDB["unitOdometer"]);
            $res[4] = $fns->nextOdometerMi($resultDB["Odometer"],$resultDB["unitOdometer"]);
            echo json_encode($res);
        }
    break;
    case 'DdlOdometerType':
        $output = $fns->getListFull(
            $sqlOps, 
            "CALL catalog_list('unitOdometer')", 
            "ddl_odometer", 
            $_POST['title'], 
            "nameCatalog", 
            "nameCatalog");
    break;
    case 'saveServices':
        $memoryServices = $_POST['memoryServices'];
        $memoryParts = $_POST['memoryParts'];
        $idVehicle = $_POST['idVehicle'];
        $totalCost = $_POST['totalCost'];
        
        $sqlMeta = "CALL metaservice_add("
            .$idVehicle
            .",'".$memoryServices[0]['FechaAhora']."'"
            .",'".$memoryServices[0]['OdometroAhora']."'"
            .",'".$memoryServices[0]['TipoOdometro']."'"
            .",'".$totalCost."'"
            . ",@si)";
        $idMeta = $sqlOps->sql_exec_op_return($sqlMeta);    
            
        foreach ($memoryServices as &$rowMem) {
            $sqlService = "CALL service_add("
                ."'".$rowMem['idServicio']."'"
                .",'".$rowMem['FechaSiguiente']."'"
                .",'".$rowMem['OdometroSiguiente']."'"
                .",'".$rowMem['Costo']."'"
                .",'".$rowMem['Nota']."'"
                .",'".$rowMem['Alerta']."'"
                .",'".$idMeta."'"                    
                . ",@si)";
            $idService = $sqlOps->sql_exec_op_return($sqlService);    
            
            foreach ($memoryParts as &$rowInt) {
                if($rowInt['Servicio']==$rowMem['idServicio']){
                    $sqlDetail = "CALL servicedetail_add("
                        ."'".$rowInt['Parte']."'"
                        .",'".$rowInt['Codigo']."'"
                        .",'".$rowInt['Precio']."'"
                        .",'".$rowInt['Marca']."'"
                        .",'".$rowInt['Nota']."'"
                        .",'".$idService."'"                    
                        . ",@si)";
                    $idDetail = $sqlOps->sql_exec_op_return($sqlDetail);
                    $output = $idMeta;
                }
            }
        }
    break;
}
echo $output == '' ? '' : $output;