<?php
    include "../../Model/SqlOperations.php";
    $sqlOps = new SqlOperations();      
    include "../../Controller/php/general.php";
    $fns = new generalFunctions();
    $output = '';
    $type_data = isset($_POST['type']) ? $_POST['type'] : '';
    switch ($type_data)
    {
        //initial show
        case 'list_service_type':
            $result = $sqlOps->sql_multiple_rows("CALL servicetype_list()");
            $count = $result ? mysqli_num_rows($result) : -1;
            if($count > 0){
                $list = '';
                while($row = $result->fetch_assoc()){
                    $list .= '
                        <tr>
                            <td style="cursor:pointer;" data-id0="'.$row["idServiceType"].'" class="ServiceTypeClass">'.$row["nameServiceType"].'</td>
                            <td>'.$fns->getPluralNames($row["timeServiceType"], $row["unitTime"]).'</td>
                            <td>'.$row["Odometer"].' - '.$row["unitOdometer"].'</td>
                        </tr>';
                }
                $output .= '
                <table width="100%" class="table table-condensed table-bordered table-hover" id="dataTables-example" style="font-size: 12px; text-align:center;">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Nombre del Servicio</th>
                            <th style="text-align:center;">Tiempo</th>
                            <th style="text-align:center;">Odometro</th>        
                        </tr>
                    </thead>
                    <tbody>';
                $output .= $list;
                $output .= '    
                        </tbody>
                    </table>';
            }
        break;
        //get
        case 'show_service_type':
            $sql = "CALL servicetype_get(".$_POST['idServiceType'].")";
            $row = $sqlOps->sql_single_row($sql);
            if($row == ''){
                $output = '';
            }else{
                $res = array();
                $res[0][0] = $row["idServiceType"];
                $res[0][1] = $row["nameServiceType"];
                $res[0][2] = $row["timeServiceType"];
                $res[0][3] = $row["unitTime"];
                $res[0][4] = $row["Odometer"];
                $res[0][5] = $row["unitOdometer"];
                echo json_encode($res);
            }
        break;
        case 'list_time':
            $output = $fns->getListFull(
                $sqlOps, 
                "CALL catalog_list('unitServiceTime')", 
                "ddl_time_service", 
                $_POST['title'], 
                "idCatalog", 
                "nameCatalog");
        break;
        case 'list_odometer':
            $output = $fns->getListFull(
                $sqlOps, 
                "CALL catalog_list('unitOdometer')", 
                "ddl_unit_odometer", 
                $_POST['title'], 
                "idCatalog", 
                "nameCatalog");
        break;
        //delete
        case 'remove_service_type':
            $sql = "CALL servicetype_delete(".$_POST['idServiceType'].")";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        //update
        case 'update_service_type':
            $sql = "CALL servicetype_update('".$_POST['id']."','".$_POST['column_name']."','".$_POST['texto']."')";
            $output = $sqlOps->sql_exec_op($sql);
        break;
        //save
        case 'add_service_type':
            $sql = "CALL servicetype_add("
                . "'".$_POST['name']."'"
                . ",'".$_POST['time']."'"
                . ",'".$_POST['unitTime']."'"
                . ",'".$_POST['odometer']."'"
                . ",'".$_POST['unitOdometer']."'"
                . ",@si)";
            $output = $sqlOps->sql_exec_op_return($sql);            
        break;
    }
    echo $output == '' ? '' : $output;