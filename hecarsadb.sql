-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 11-06-2017 a las 09:53:36
-- Versión del servidor: 10.1.10-MariaDB
-- Versión de PHP: 5.6.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `hecarsadb`
--

DELIMITER $$
--
-- Procedimientos
--
CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `calendar_list` (IN `date_IN` DATE, IN `type_IN` VARCHAR(100))  BEGIN
    SET lc_time_names = 'es_GT';
      
    IF STRCMP(type_IN, 'General') = 0 THEN
        SELECT 
        Fecha
        ,Tipo
        FROM (
          SELECT 
            CONCAT(DATE_FORMAT(NOW(),'%Y'),'-',DATE_FORMAT(Birthday,'%m-%d')) AS Fecha
            ,'Cumplea?os' AS Tipo
          FROM person
          WHERE DATE_FORMAT(Birthday, '%m-%d') != '00-00'
            UNION ALL
          SELECT 
            DATE_FORMAT(S.nextTimeService,'%Y-%m-%d')
            ,ST.nameServiceType
          FROM metaservice AS MS
          INNER JOIN service AS S
            ON MS.idMetaService = S.idMetaService
          INNER JOIN servicetype AS ST
            ON ST.idServiceType = S.idServiceType
            ORDER BY Fecha
        ) AS T;
    ELSEIF STRCMP(type_IN, 'Details') = 0 THEN
        SELECT 
        Fecha
        ,DATE_FORMAT(Fecha,'%M') AS Mes
        ,DATE_FORMAT(Fecha,'%Y') AS Anio
        ,DATE_FORMAT(Fecha,'%W') AS DiaNombre
        ,DATE_FORMAT(Fecha,'%d') AS Dia
        ,Tipo
        ,Persona
        ,Telefono
        FROM (
          SELECT 
            CONCAT(DATE_FORMAT(NOW(),'%Y'),'-',DATE_FORMAT(Birthday,'%m-%d')) AS Fecha
            ,'Cumplea?os' AS Tipo
            ,namePerson AS Persona
            ,Phone AS Telefono
          FROM person
            UNION
          SELECT 
            CONCAT(DATE_FORMAT(NOW(),'%Y'),'-',DATE_FORMAT(S.nextTimeService,'%m-%d')) AS Fecha
            ,CONCAT(ST.nameServiceType,'\n',VPC.brand)
            ,P.namePerson
            ,P.Phone
          FROM service AS S
          INNER JOIN metaservice AS MS
            ON MS.idMetaService = S.idMetaService
          INNER JOIN vehicleperclient AS VPC
            ON MS.idVehicle = VPC.idVehiclePerClient
          INNER JOIN person AS P
            ON VPC.idPerson = P.idPerson
          INNER JOIN servicetype AS ST
            ON ST.idServiceType = S.idServiceType
            ORDER BY Fecha
        ) AS T
        WHERE Fecha = date_IN;
    ELSEIF STRCMP(type_IN, 'ForToday') = 0 THEN
        
        SET @NoServices = NULL;
        SET @NoBirthdays = NULL;
        
        SET @NoServices = (
            SELECT COUNT(*)
            FROM service
            WHERE DATE_FORMAT(nextTimeService,'%Y-%m-%d') = 
            DATE_FORMAT(date_IN,'%Y-%m-%d')
        );
        
        SET @NoBirthdays = (
            SELECT COUNT(*)
            FROM person
            WHERE 
            CONCAT(DATE_FORMAT(NOW(),'%Y'),'-',DATE_FORMAT(Birthday,'%m-%d')) 
            = 
            DATE_FORMAT(date_IN,'%Y-%m-%d')
        );
        
        IF(@NoServices > 0 OR @NoBirthdays > 0) THEN
            SELECT 
            DATE_FORMAT(date_IN,'%M') AS Mes
            ,DATE_FORMAT(date_IN,'%Y') AS Anio
            ,DATE_FORMAT(date_IN,'%W') AS DiaNombre
            ,DATE_FORMAT(date_IN,'%d') AS Dia
            ,(@NoServices+@NoBirthdays) AS Data;
        ELSE
            SELECT 
            DATE_FORMAT(date_IN,'%M') AS Mes
            ,DATE_FORMAT(date_IN,'%Y') AS Anio
            ,DATE_FORMAT(date_IN,'%W') AS DiaNombre
            ,DATE_FORMAT(date_IN,'%d') AS Dia
            ,0 AS Data;
        END IF;    
    END IF;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `catalog_add` (`nameCatalog_IN` VARCHAR(200), `nameMetaCatalog_IN` VARCHAR(200), OUT `last_id` INT(11))  BEGIN
    INSERT INTO catalog
      (nameCatalog, idMeta) 
    VALUES (
      nameCatalog_IN, 
      getMetaId(nameMetaCatalog_IN));
    SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `catalog_delete` (`idCatalog_IN` INT(11))  BEGIN
    DELETE FROM catalog
    WHERE idCatalog = idCatalog_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `catalog_list` (`meta_IN` VARCHAR(50))  BEGIN
    SELECT idCatalog, nameCatalog
        FROM catalog
    WHERE idMeta = getMetaId(meta_IN)
    ORDER BY nameCatalog;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `catalog_update` (`idCatalog_IN` INT(11), `nameCatalog_IN` VARCHAR(200))  BEGIN
    UPDATE catalog 
      SET nameCatalog = nameCatalog_IN
    WHERE idCatalog = idCatalog_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `metaservice_add` (IN `idVehicle_IN` INT(11), IN `currentTimeService_IN` VARCHAR(25), IN `currentOdometer_IN` INT(11), IN `TypeOdometer_IN` VARCHAR(3), IN `totalCost_IN` INT(11), OUT `last_id` INT(11))  BEGIN
    INSERT INTO metaservice(
        idVehicle
        ,currentTimeService
        ,currentOdometer
        ,TypeOdometer
        ,totalCost) 
    VALUES (
        idVehicle_IN
        ,DATE_FORMAT(
          STR_TO_DATE(
            CONCAT(
              currentTimeService_IN
              ,' '
              ,DATE_FORMAT(NOW(),'%h:%i %p')
            ),'%d-%m-%Y %h:%i %p')
          ,'%Y-%m-%d %h:%i %p')
        ,currentOdometer_IN
        ,getCategoryId(TypeOdometer_IN)
        ,totalCost_IN);
    SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `partperservice_add` (IN `idPart_IN` INT(11), IN `idServiceType_IN` INT(11))  BEGIN
  INSERT INTO partperservicetype
    (idPart, idServiceType) 
  VALUES (idPart_IN, idServiceType_IN);
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `partperservice_assigned_list` (IN `categoryId_IN` INT(11))  BEGIN
  SELECT PPS.idPartPerServiceType
  ,PPS.idPart
  ,P.namePart
  ,getCategoryName(P.categoryPart) AS categoryName
  ,P.imageUrl
    FROM partperservicetype AS PPS
    INNER JOIN part AS P
    ON PPS.idPart = P.idPart
  WHERE PPS.idServiceType = categoryId_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `partperservice_list` ()  BEGIN
  SELECT idServiceType, nameServiceType
  FROM servicetype;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `partperservice_NotAssigned_list` (IN `categoryId_IN` INT(11))  BEGIN
  SELECT 
    P.idPart
    ,P.namePart
    ,getCategoryName(P.categoryPart) AS categoryName
    ,P.imageUrl
  FROM part AS P
  WHERE P.idPart NOT IN (
    SELECT PPS.idPart
    FROM partperservicetype AS PPS
    WHERE PPS.idServiceType = categoryId_IN
  );
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `partperservice_remove` (IN `idPart_IN` INT(11), IN `idServiceType_IN` INT(11))  BEGIN
  DELETE FROM partperservicetype
  WHERE idPart = idPart_IN
  AND idServiceType = idServiceType_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `partpervehicle_add` (IN `idPart_IN` INT(11), IN `idVehicleType_IN` INT(11))  BEGIN
  INSERT INTO partpervehicletype
    (idPart, idVehicleType) 
  VALUES (idPart_IN, idVehicleType_IN);
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `partpervehicle_assigned_list` (IN `categoryId_IN` INT(11))  BEGIN
  SELECT PPV.idPartPerVehicleType
  ,PPV.idPart
  ,P.namePart
  ,getCategoryName(P.categoryPart) AS categoryName
  ,P.imageUrl
    FROM partpervehicletype AS PPV
    INNER JOIN part AS P
    ON PPV.idPart = P.idPart
  WHERE PPV.idVehicleType = categoryId_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `partpervehicle_NotAssigned_list` (IN `categoryId_IN` INT(11))  BEGIN
  SELECT 
    P.idPart
    ,P.namePart
    ,getCategoryName(P.categoryPart) AS categoryName
    ,P.imageUrl
  FROM part AS P
  WHERE P.idPart NOT IN (
    SELECT PPV.idPart
    FROM partpervehicletype AS PPV
    WHERE PPV.idVehicleType = categoryId_IN
  );
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `partpervehicle_remove` (IN `idPart_IN` INT(11), IN `idVehicleType_IN` INT(11))  BEGIN
  DELETE FROM partpervehicletype
  WHERE idPart = idPart_IN
  AND idVehicleType = idVehicleType_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `part_add` (IN `namePart_IN` VARCHAR(200), IN `idCategoryPart_IN` INT(11), IN `imageUrl_IN` VARCHAR(20), OUT `last_id` INT(11))  BEGIN
  INSERT INTO part
    (namePart, categoryPart, imageUrl) 
  VALUES (
    namePart_IN, 
    idCategoryPart_IN, 
    imageUrl_IN);
  SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `part_delete` (IN `idPart_IN` INT(11))  BEGIN
  DELETE FROM part 
    WHERE idPart = idPart_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `part_get` (IN `idPart_IN` INT(11))  BEGIN
  SELECT idPart, 
      namePart, 
      categoryPart, 
      getCategoryName(categoryPart) as categoryName,
      imageUrl 
  FROM part
  WHERE idPart = idPart_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `part_list` ()  BEGIN
  SELECT idPart, 
      namePart, 
      categoryPart, 
      getCategoryName(categoryPart) as categoryName,
      imageUrl 
  FROM part;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `part_service_list` (IN `idService_IN` INT(11), IN `idPart_IN` INT(11))  BEGIN
  SELECT 
    idService_IN AS serviceId
    ,getServiceTypeName(idService_IN) AS serviceName
    ,idPart
    ,namePart
    ,imageUrl
  FROM part
    WHERE idPart = idPart_IN;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `part_update` (IN `idPart_IN` INT(11), IN `column_name_IN` VARCHAR(15), IN `texto_IN` VARCHAR(200))  BEGIN
  SET @QUERY = CONCAT(
    'UPDATE part', 
    ' SET ', column_name_IN, ' = ');
  SET @QUERY = CONCAT(@QUERY,'''',texto_IN,'''');
  SET @QUERY = CONCAT(@QUERY,' WHERE idPart = ', idPart_IN);
  PREPARE stmt FROM @QUERY;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `person_add` (IN `legalName_IN` VARCHAR(200), IN `namePerson_IN` VARCHAR(200), IN `NIT_IN` INT(11), IN `Phone_IN` INT(11), IN `Birthday_IN` VARCHAR(20), OUT `last_id` INT(11))  BEGIN
  INSERT INTO person
      (legalName, namePerson, NIT, Phone, Birthday) 
  VALUES (
      legalName_IN,
      namePerson_IN,
      NIT_IN,
      Phone_IN,
      DATE_FORMAT(
        STR_TO_DATE(
          CONCAT(
            Birthday_IN
            ,'/'
            ,DATE_FORMAT(NOW(),'%Y')
          )
        ,'%d/%m/%Y')
      ,'%Y-%m-%d')
  );
  SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `person_autocomplete` (IN `namePerson_IN` VARCHAR(10))  BEGIN
  SELECT idPerson, 
      namePerson 
  FROM person
  WHERE LOWER(namePerson)
  LIKE LOWER(CONCAT('%',namePerson_IN,'%'))
  ORDER BY namePerson;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `person_delete` (IN `idPerson_IN` INT(11))  BEGIN
  DELETE FROM person 
    WHERE idPerson = idPerson_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `person_get` (IN `idPerson_IN` INT(11))  BEGIN
  SELECT idPerson, 
      legalName, 
      namePerson, 
      NIT, 
      Phone,
      DATE_FORMAT(Birthday,'%d/%m/%Y') AS Birthday
  FROM person
  WHERE idPerson = idPerson_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `person_list` ()  BEGIN
  SET lc_time_names = 'es_GT';
  
  SELECT idPerson, 
      legalName, 
      namePerson, 
      NIT, 
      Phone, 
      CONCAT(
        DATE_FORMAT(Birthday,'%d'),
        ' de ',
        DATE_FORMAT(Birthday,'%M')
      ) AS Birthday
  FROM person;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `person_update` (IN `idPerson_IN` INT(11), IN `column_name_IN` VARCHAR(15), IN `texto_IN` VARCHAR(200))  BEGIN
  SET @QUERY = CONCAT(
    'UPDATE person', 
    ' SET ', column_name_IN, ' = ');
  IF STRCMP(column_name_IN, 'Birthday') = 0 THEN
    SET @QUERY = CONCAT(
                  @QUERY
                  ,'DATE_FORMAT(STR_TO_DATE(CONCAT(''',texto_IN,''',''/'',DATE_FORMAT(NOW(),''%Y'')),''%d/%m/%Y''),''%Y-%m-%d'')');
  ELSE
    SET @QUERY = CONCAT(@QUERY,'''',texto_IN,'''');
  END IF; 
  SET @QUERY = CONCAT(@QUERY,' WHERE idPerson = ', idPerson_IN);
  #SELECT @QUERY;
  PREPARE stmt FROM @QUERY;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `plate_autocomplete` (IN `plateNumber_IN` VARCHAR(10))  BEGIN
  SELECT idperson, 
      plateNumber 
  FROM vehicleperclient
  WHERE LOWER(plateNumber)
  LIKE LOWER(CONCAT('%',plateNumber_IN,'%'))
  ORDER BY plateNumber_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `resumevehicle_add` (`idvehicle_IN` INT(11), `idPart_IN` INT(11), `codePart_IN` VARCHAR(100), `brandPart_IN` VARCHAR(100), OUT `last_id` INT(11))  BEGIN
    INSERT INTO resumevehicle(
      idVehicle
      ,idPart
      ,codePart
      ,brandPart) 
    VALUES (
      idvehicle_IN
      ,idPart_IN
      ,codePart_IN
      ,brandPart_IN);
    SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `resumevehicle_delete` (`idResume_IN` INT(11))  BEGIN
    DELETE FROM resumevehicle 
    WHERE idResume = idResume_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `resumevehicle_list_IN` (`idvehicle_IN` INT(11))  BEGIN
    SELECT 
      RV.idResume
      ,P.idPart
      ,P.namePart
      ,P.imageUrl
      ,RV.codePart
      ,RV.brandPart
    FROM resumevehicle AS RV
    INNER JOIN part AS P
    ON RV.idPart = P.idPart
      WHERE RV.idVehicle = idvehicle_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `resumevehicle_list_NOT` (`idvehicle_IN` INT(11))  BEGIN
    SELECT P.idPart, 
      P.namePart, 
      P.imageUrl 
    FROM part AS P
    WHERE P.idPart NOT IN (
        SELECT RV.idPart
          FROM resumevehicle AS RV
        WHERE RV.idVehicle = idvehicle_IN
    );
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `resumevehicle_memory` (`idListPart_IN` VARCHAR(200), `type_IN` VARCHAR(200))  BEGIN
    SET @QUERY = CONCAT(
    'SELECT idPart,namePart,imageUrl',
    ' FROM part');
    IF STRCMP(type_IN, 'IN') = 0 THEN
      SET @QUERY = CONCAT(@QUERY,' WHERE idPart IN (',idListPart_IN,')');
    ELSEIF STRCMP(type_IN, 'NOT') = 0 THEN
      SET @QUERY = CONCAT(@QUERY,' WHERE idPart NOT IN (',idListPart_IN,')');
    ELSEIF STRCMP(type_IN, 'ALL') = 0 THEN
      SET @QUERY = CONCAT(@QUERY);
    END IF;
    #SELECT @QUERY;
    PREPARE stmt FROM @QUERY;
    EXECUTE stmt;
    DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `resumevehicle_update` (IN `idResume_IN` INT(11), IN `column_name_IN` VARCHAR(15), IN `texto_IN` VARCHAR(200))  BEGIN
  SET @QUERY = CONCAT(
    'UPDATE resumevehicle', 
    ' SET ', column_name_IN, ' = ','''',texto_IN,'''',
    ' WHERE idResume = ', idResume_IN);
  #SELECT @QUERY;
  PREPARE stmt FROM @QUERY;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `servicedetail_add` (IN `idPart_IN` INT(11), IN `codePart_IN` VARCHAR(100), IN `PricePart_IN` FLOAT, IN `brandPart_IN` VARCHAR(100), IN `Notes_IN` VARCHAR(200), IN `idService_IN` INT(11), OUT `last_id` INT(11))  BEGIN
    INSERT INTO servicedetail(
        idPart
        ,codePart
        ,pricePart
        ,brandPart
        ,note
        ,idService) 
    VALUES (
        idPart_IN
        ,codePart_IN
        ,PricePart_IN
        ,brandPart_IN
        ,Notes_IN
        ,idService_IN);
    SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `servicedetail_get` (IN `idService_IN` INT(11))  BEGIN
    SELECT 
    SD.codePart
    ,P.namePart
    ,P.imageUrl
    ,SD.PricePart
    ,SD.brandPart
    ,SD.note
        FROM servicedetail AS SD
        INNER JOIN part AS P 
        ON P.idPart = SD.idPart
    WHERE idService = idService_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `servicedetail_list` ()  BEGIN
    SELECT idServiceDetail
    ,idService
    ,idPart
    ,PricePart
    ,codePart
    ,brandPart
    ,Other 
        FROM servicedetail;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `servicedetail_remove` (IN `idServiceDetail_IN` INT(11))  BEGIN
    DELETE FROM servicedetail
    WHERE idServiceDetail = idServiceDetail_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `servicetype_add` (IN `nameServiceType_IN` VARCHAR(100), IN `timeServiceType_IN` INT(11), IN `unitTime_IN` INT(11), IN `Odometer_IN` INT(11), IN `unitOdometer_IN` INT(11), OUT `last_id` INT(11))  BEGIN
  INSERT INTO hecarsadb.servicetype(
    nameServiceType
    ,timeServiceType
    ,unitTime
    ,Odometer
    ,unitOdometer
  ) 
  VALUES (
    nameServiceType_IN
    ,timeServiceType_IN
    ,unitTime_IN
    ,Odometer_IN
    ,unitOdometer_IN
  );
  SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `servicetype_delete` (IN `idServiceType_IN` INT(11))  BEGIN
  DELETE FROM servicetype  
    WHERE idServiceType = idServiceType_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `servicetype_get` (IN `idServiceType_IN` INT(11))  BEGIN
  SELECT idServiceType
  ,nameServiceType
  ,DATE_FORMAT(NOW(),'%d-%m-%Y') AS currentDateTime
  ,timeServiceType
  ,unitTime AS idUnitTime
  ,getCategoryName(unitTime) AS unitTime
  ,Odometer
  ,unitOdometer AS idUnitOdometer
  ,getCategoryName(unitOdometer) AS unitOdometer 
    FROM servicetype
  WHERE idServiceType = idServiceType_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `servicetype_list` ()  BEGIN
  SELECT idServiceType
  ,nameServiceType
  ,timeServiceType
  ,unitTime AS idUnitTime
  ,getCategoryName(unitTime) AS unitTime
  ,Odometer
  ,unitOdometer AS idUnitOdometer
  ,getCategoryName(unitOdometer) AS unitOdometer 
    FROM servicetype;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `servicetype_update` (IN `idServiceType_IN` INT(11), IN `column_name_IN` VARCHAR(15), IN `texto_IN` VARCHAR(100))  BEGIN
  SET @QUERY = CONCAT(
    'UPDATE servicetype', 
    ' SET ', column_name_IN, ' = ');
  SET @QUERY = CONCAT(@QUERY,'''',texto_IN,'''');
  SET @QUERY = CONCAT(@QUERY,' WHERE idServiceType = ', idServiceType_IN);
  
  PREPARE stmt FROM @QUERY;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `service_add` (IN `idServiceType_IN` INT(11), IN `nextTimeService_IN` VARCHAR(25), IN `nextOdometer_IN` INT(11), IN `cost_IN` FLOAT, IN `Notes_IN` VARCHAR(200), IN `Alerts_IN` VARCHAR(200), IN `idMetaService_IN` INT(11), OUT `last_id` INT(11))  BEGIN
    INSERT INTO service(
        idServiceType
        ,nextTimeService
        ,nextOdometer
        ,cost
        ,note
        ,alert
        ,idMetaService) 
    VALUES (
        idServiceType_IN
        ,DATE_FORMAT(STR_TO_DATE(CONCAT(nextTimeService_IN,' ',DATE_FORMAT(NOW(),'%h:%i %p')),'%d-%m-%Y %h:%i %p'),'%Y-%m-%d %h:%i %p')
        ,nextOdometer_IN
        ,cost_IN
        ,Notes_IN
        ,Alerts_IN
        ,idMetaService_IN);
    SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `service_ByVehicle` (IN `idVehicle_IN` INT(11))  BEGIN
    SET lc_time_names = 'es_GT';
    SELECT 
    S.idService
    ,DATE_FORMAT(MS.currentTimeService,'%d-%M-%Y %h:%i %p') AS currentTimeService
    ,getServiceTypeName(S.idServiceType) AS nameServiceType
    ,REPLACE(FORMAT(MS.currentOdometer, 2),'.00', '') AS currentOdometer
    ,getCategoryName(MS.TypeOdometer) AS nameOdometerType
    ,S.cost
    ,S.note
      FROM metaservice AS MS
      INNER JOIN service AS S 
      ON MS.idMetaService = S.idMetaService
    WHERE MS.idVehicle = idVehicle_IN
    ORDER BY MS.currentTimeService DESC;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `service_get` (IN `idService_IN` INT(11))  BEGIN
    SELECT S.idService
    ,MS.currentTimeService
    ,S.nextTimeService
    ,getCategoryName(S.idServiceType) AS nameServiceType
    ,MS.currentOdometer
    ,S.nextOdometer
    ,getCategoryName(MS.TypeOdometer) AS nameOdometerType
    ,S.cost
    ,S.note
    ,S.alert
        FROM service AS S
        INNER JOIN metaservice AS MS
        ON S.idMetaService = MS.idMetaService
    WHERE S.idService = idService_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `service_getAlerts` (IN `idService_IN` INT(11))  BEGIN
    SELECT alert
        FROM service
    WHERE idService = idService_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `service_list` ()  BEGIN
    SELECT idService
    ,idVehicle
    ,idServiceType
    ,TypeOdometer
    ,currentOdometer
    ,nextOdometer
    ,TypeTimeService
    ,currentTimeService
    ,nextTimeService
    ,cost
    ,Notes
    ,Alerts 
        FROM service;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `service_remove` (IN `idService_IN` INT(11))  BEGIN
    DELETE FROM service
    WHERE idService = idService_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `vehicleperclient_add` (`idPerson_IN` INT(11), `idVehicleType_IN` INT(11), `PlateNumber_IN` VARCHAR(50), `Brand_IN` VARCHAR(50), `Line_IN` VARCHAR(50), `Color_IN` VARCHAR(50), `YearVehicle_IN` INT(11), OUT `last_id` INT(11))  BEGIN
    INSERT INTO hecarsadb.vehicleperclient
        (idPerson, 
        idVehicleType, 
        PlateNumber, 
        Brand, 
        Line, 
        Color, 
        YearVehicle) 
    VALUES 
        (idPerson_IN, 
        idVehicleType_IN, 
        PlateNumber_IN, 
        Brand_IN, 
        Line_IN, 
        Color_IN, 
        YearVehicle_IN);
    SET last_id = LAST_INSERT_ID();
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `vehicleperclient_get` (`idvehicleperclient_IN` INT(11))  BEGIN
    SELECT getPersonName(idPerson) AS namePerson
        ,getCategoryName(idVehicleType) AS nameVehicleType
        ,PlateNumber
        ,Brand
        ,Line
        ,Color
        ,YearVehicle 
    FROM vehicleperclient
    WHERE idvehicleperclient = idvehicleperclient_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `vehicleperclient_getByPerson` (`idPerson_IN` INT(11))  BEGIN
    SELECT idvehicleperclient, plateNumber, brand, color
    FROM vehicleperclient
    WHERE idPerson = idPerson_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `vehicleperclient_list` ()  BEGIN
    SELECT idvehicleperclient
        ,idPerson
        ,getPersonName(idPerson) AS namePerson
        ,idVehicleType
        ,getCategoryName(idVehicleType) AS nameVehicleType
        ,PlateNumber
        ,Brand
        ,Line
        ,Color
        ,YearVehicle 
    FROM vehicleperclient;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `vehicleperclient_remove` (`idvehicleperclient_IN` INT(11))  BEGIN
    DELETE FROM vehicleperclient 
    WHERE idvehicleperclient 
    = idvehicleperclient_IN;
END$$

CREATE DEFINER=`leoncio`@`localhost` PROCEDURE `vehicleperclient_update` (IN `idvehicleperclient_IN` INT(11), IN `column_name_IN` VARCHAR(15), IN `texto_IN` VARCHAR(100))  BEGIN
  SET @QUERY = CONCAT(
    'UPDATE vehicleperclient', 
    ' SET ', column_name_IN, ' = ');
  SET @QUERY = CONCAT(@QUERY,'''',texto_IN,'''');
  SET @QUERY = CONCAT(@QUERY,' WHERE idvehicleperclient = ', idvehicleperclient_IN);
  
  PREPARE stmt FROM @QUERY;
  EXECUTE stmt;
  DEALLOCATE PREPARE stmt;
END$$

--
-- Funciones
--
CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getCategoryId` (`categoryName_IN` VARCHAR(100)) RETURNS INT(11) BEGIN
  
  SET @idObj = NULL;
  
  SELECT idCatalog INTO @idObj
      FROM catalog
  WHERE nameCatalog = categoryName_IN;
  
	RETURN @idObj;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getCategoryName` (`category_IN` INT(11)) RETURNS VARCHAR(100) CHARSET latin1 BEGIN
  
  SET @nameObj = NULL;
  
  SELECT nameCatalog INTO @nameObj
      FROM catalog
  WHERE idCatalog = category_IN;
  
	RETURN @nameObj;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getMetaId` (`metaName_IN` VARCHAR(100)) RETURNS INT(11) BEGIN

  SET @idMeta = NULL;

  SELECT MC.idMeta INTO @idMeta
      FROM metacatalog AS MC
  WHERE MC.nameMeta = metaName_IN;
  
	RETURN @idMeta;
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getPersonName` (`idPerson_IN` INT(11)) RETURNS VARCHAR(100) CHARSET latin1 BEGIN
  
  SET @nameObj = NULL;
  
  SELECT namePerson INTO @nameObj
  FROM person
  WHERE idPerson = idPerson_IN;
  
	RETURN @nameObj;
  
END$$

CREATE DEFINER=`leoncio`@`localhost` FUNCTION `getServiceTypeName` (`id_IN` INT(11)) RETURNS VARCHAR(100) CHARSET latin1 BEGIN
  
  SET @nameObj = NULL;
   
  SELECT nameServiceType INTO @nameObj
      FROM servicetype
  WHERE idServiceType = id_IN;
  
	RETURN @nameObj;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `catalog`
--

CREATE TABLE `catalog` (
  `idCatalog` int(11) NOT NULL,
  `nameCatalog` varchar(200) DEFAULT NULL,
  `idMeta` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `catalog`
--

INSERT INTO `catalog` (`idCatalog`, `nameCatalog`, `idMeta`) VALUES
(1, 'bigunote', 1),
(2, 'medium', 1),
(3, 'small', 1),
(4, 'Picop', 2),
(5, 'Sedan', 2),
(6, 'SUV', 2),
(7, 'Hatchback', 2),
(40, 'Mayor', 3),
(41, 'Menor', 3),
(42, 'Ajuste', 3),
(44, 'Lavado', 3),
(49, 'Pintura', 3),
(50, 'Dia', 4),
(51, 'Semana', 4),
(52, 'Mes', 4),
(53, 'Año', 4),
(54, 'Km', 5),
(55, 'Mi', 5),
(57, 'ipsum', 1),
(58, 'chuchito', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metacatalog`
--

CREATE TABLE `metacatalog` (
  `idMeta` int(11) NOT NULL,
  `nameMeta` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `metacatalog`
--

INSERT INTO `metacatalog` (`idMeta`, `nameMeta`) VALUES
(1, 'PartCategory'),
(2, 'VehicleType'),
(3, 'ServiceType'),
(4, 'unitServiceTime'),
(5, 'unitOdometer');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `metaservice`
--

CREATE TABLE `metaservice` (
  `idMetaService` int(11) NOT NULL,
  `idVehicle` int(11) DEFAULT NULL,
  `currentTimeService` datetime DEFAULT NULL,
  `currentOdometer` int(11) DEFAULT NULL,
  `TypeOdometer` int(11) DEFAULT NULL,
  `totalCost` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `metaservice`
--

INSERT INTO `metaservice` (`idMetaService`, `idVehicle`, `currentTimeService`, `currentOdometer`, `TypeOdometer`, `totalCost`) VALUES
(1, 21, '2017-01-23 21:03:13', 137875, 54, 145.15),
(4, 21, '2017-03-02 21:03:13', 137875, 54, 145.14),
(5, 8, '2017-04-28 21:03:13', 146950, 55, 175.15),
(7, 8, '2017-01-12 21:03:13', 146950, 55, 175.29),
(13, 23, '2017-02-19 21:03:13', 167461, 55, 209.68),
(14, 25, '2017-04-23 21:45:47', 7984, 54, 164),
(17, 25, '2017-04-23 22:08:42', 3243, 54, 718),
(18, -1, '2017-04-27 21:47:00', 0, NULL, 0),
(19, 4, '2017-04-27 22:06:18', 78985, 54, 178),
(21, 4, '2017-04-29 10:34:24', 78, 55, 579),
(22, 4, '2017-04-29 10:51:13', 5675, 54, 67),
(23, 4, '2017-04-29 10:58:27', 0, NULL, 789),
(24, 11, '2017-04-29 11:00:10', 0, NULL, 888),
(25, 24, '2017-04-29 11:04:32', 543543, 54, 721),
(26, 4, '2017-04-29 11:07:35', 676, 55, 1323),
(27, 11, '2017-05-01 21:05:22', 1000, 55, 510),
(28, 22, '2017-05-01 21:07:07', 4354, 55, 159),
(29, 9, '2017-05-01 21:15:23', 0, NULL, 35),
(30, 9, '2017-05-01 21:20:19', 0, NULL, 78),
(31, 16, '2017-05-08 22:51:25', 555, 55, 54),
(32, 14, '2017-05-08 22:52:21', 888, 54, 999),
(33, 22, '2017-05-20 14:35:02', 0, 54, 78),
(34, 21, '2017-05-20 14:39:56', 45, 54, 111),
(35, 4, '2017-05-31 00:00:00', 444, 54, 555),
(36, 4, '2017-02-14 02:49:00', 444, 54, 555),
(37, 22, '2017-02-14 02:52:00', 3344, 54, 45),
(38, 19, '2017-05-20 02:54:00', 666, 54, 5555),
(39, 22, '2017-03-01 08:39:00', 78, 54, 99);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `part`
--

CREATE TABLE `part` (
  `idPart` int(11) NOT NULL,
  `namePart` varchar(200) DEFAULT NULL,
  `categoryPart` int(11) DEFAULT NULL,
  `imageUrl` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `part`
--

INSERT INTO `part` (`idPart`, `namePart`, `categoryPart`, `imageUrl`) VALUES
(26, 'llave', 3, '1.jpg'),
(27, 'simply', 3, '2.jpg'),
(28, 'spray', 3, '3.jpg'),
(29, 'capó', 3, '4.jpg'),
(31, 'palanca', 1, '6.jpg'),
(32, 'standard', 3, '7.jpg'),
(33, 'shock gigante', 2, '33215.png'),
(34, 'radiador', 2, '34567.png'),
(35, 'gas', 3, '10.jpg'),
(36, 'bateria', 2, '11.jpg'),
(37, 'frenos de disco', 1, '12.jpg'),
(38, 'filtros de aire', 3, '38.jpg'),
(39, 'printer', 3, '14.jpg'),
(41, 'bumper', 3, '16.jpg'),
(42, 'pistones', 1, '17.jpg'),
(43, 'luces', 1, '18.jpg'),
(44, 'ventilador', 2, '19.jpg'),
(45, 'aros', 2, '20.jpg'),
(46, 'retrovisor', 3, '21.jpg'),
(47, 'escape', 2, '47.jpg'),
(48, 'five', 2, '23.jpg'),
(49, 'candelas', 1, '24.jpg'),
(50, 'piston', 3, '50.jpg'),
(51, 'motor', 1, '51887.jpg'),
(59, 'chuchito', 1, '59998.png'),
(60, 'avioncito', 1, '60671.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partperservicetype`
--

CREATE TABLE `partperservicetype` (
  `idPartPerServiceType` int(11) NOT NULL,
  `idPart` int(11) DEFAULT NULL,
  `idServiceType` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `partperservicetype`
--

INSERT INTO `partperservicetype` (`idPartPerServiceType`, `idPart`, `idServiceType`) VALUES
(4, 44, 13),
(5, 43, 14),
(6, 38, 16),
(10, 35, 13),
(12, 33, 14),
(13, 27, 16),
(16, 32, 13),
(17, 36, 13),
(18, 27, 14),
(19, 28, 14),
(20, 29, 16),
(21, 27, 16),
(22, 27, 10),
(23, 28, 10),
(24, 29, 10),
(25, 31, 10),
(27, 26, 16),
(28, 26, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `partpervehicletype`
--

CREATE TABLE `partpervehicletype` (
  `idPartPerVehicleType` int(11) NOT NULL,
  `idPart` int(11) DEFAULT NULL,
  `idVehicleType` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `partpervehicletype`
--

INSERT INTO `partpervehicletype` (`idPartPerVehicleType`, `idPart`, `idVehicleType`) VALUES
(5, 31, 5),
(10, 29, 5),
(12, 33, 5),
(13, 32, 5),
(14, 28, 5),
(15, 26, 5),
(16, 35, 5),
(17, 36, 5),
(18, 37, 5),
(20, 28, 7),
(21, 29, 7),
(22, 26, 4),
(23, 31, 7),
(25, 27, 4),
(27, 41, 7),
(29, 38, 5),
(30, 28, 4),
(31, 31, 6),
(32, 33, 6),
(33, 32, 6),
(34, 26, 7),
(35, 27, 7),
(36, 51, 7),
(40, 32, 7),
(41, 26, 6);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `person`
--

CREATE TABLE `person` (
  `idPerson` int(11) NOT NULL,
  `legalName` varchar(200) DEFAULT NULL,
  `namePerson` varchar(200) DEFAULT NULL,
  `NIT` int(11) DEFAULT NULL,
  `Phone` int(11) DEFAULT NULL,
  `Birthday` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `person`
--

INSERT INTO `person` (`idPerson`, `legalName`, `namePerson`, `NIT`, `Phone`, `Birthday`) VALUES
(5, 'Maggie Simpsons', 'Janella Currence', 57804488, 57804488, '1967-09-25'),
(6, 'Ingenio Pantaleon', 'Sunny Thodes', 54687775, 54687775, '1968-05-19'),
(7, 'Chandra Gassner', 'Amos Cheung', 58085974, 58085974, '1979-05-05'),
(9, 'Madge Dagley', 'Elvin Scheveson', 51011313, 51011313, '1971-12-11'),
(12, 'Marnie Singler', 'Chieko Roza', 59381030, 56353647, '1992-05-07'),
(13, 'Edmund Harriss', 'Veola Mckennon', 57416280, 50111528, '1996-09-27'),
(14, 'Burl Motz', 'Leonor Atteberies', 53007711, 52298142, '2017-11-11'),
(15, '', 'chuchito', 0, 78888, '2017-10-17'),
(17, '', 'aaaaa', 0, 78979, NULL);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `resumevehicle`
--

CREATE TABLE `resumevehicle` (
  `idResume` int(11) NOT NULL,
  `idVehicle` int(11) DEFAULT NULL,
  `idPart` int(11) DEFAULT NULL,
  `codePart` varchar(100) DEFAULT NULL,
  `brandPart` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `resumevehicle`
--

INSERT INTO `resumevehicle` (`idResume`, `idVehicle`, `idPart`, `codePart`, `brandPart`) VALUES
(3, 21, 51, 'StemV70', 'MissingWidth80'),
(5, 21, 44, 'TrueType200', 'CalligrapherRegular20'),
(13, 21, 36, 'gggg', 'eeeee'),
(20, 16, 45, '', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `service`
--

CREATE TABLE `service` (
  `idService` int(11) NOT NULL,
  `idServiceType` int(11) DEFAULT NULL,
  `nextTimeService` datetime DEFAULT NULL,
  `nextOdometer` int(11) DEFAULT NULL,
  `cost` float DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `alert` varchar(200) DEFAULT NULL,
  `idMetaService` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `service`
--

INSERT INTO `service` (`idService`, `idServiceType`, `nextTimeService`, `nextOdometer`, `cost`, `note`, `alert`, `idMetaService`) VALUES
(1, 10, '2017-06-09 19:11:00', 139964, 145.15, 'Tiled say decay spoil now walls meant house. ', 'Ask too matter formed county wicket oppose talent. ', 1),
(2, 13, '2017-06-19 16:39:00', 149343, 175.01, 'My mr interest thoughts screened of outweigh removing. ', 'As greatly replied it windows of an minuter behaved passage.', 1),
(3, 10, '2017-07-06 20:46:00', 139964, 145.14, 'At design he vanity at cousin longer looked ye.', 'Design praise me father an favour.', 4),
(4, 16, '2017-07-12 15:49:00', 149343, 175.15, 'Open know age use whom him than lady was', 'He immediate sometimes or to dependent in. ', 13),
(5, 13, '2017-07-18 03:32:00', 149343, 175.29, 'On lasted uneasy exeter my itself effect spirit.', 'Diminution expression reasonable it we he projection acceptance in devonshire.', 13),
(6, 14, '2017-08-09 08:53:00', 135684, 306.37, 'Evening society musical besides inhabit ye my. ', 'Everything few frequently discretion surrounded did simplicity decisively. ', 13),
(7, 14, '2017-09-05 09:20:00', 161537, 458.38, 'Lose hill well up will he over on. ', 'Less he year do with no sure loud. ', 5),
(8, 10, '2017-09-15 16:30:00', 111044, 350.47, 'Increasing sufficient everything men him admiration unpleasing sex. ', 'He unaffected sympathize discovered at no am conviction principles. ', 5),
(9, 16, '2017-09-18 09:16:00', 157419, 419.53, 'Around really his use uneasy longer him man. ', 'Girl ham very how yet hill four show. ', 5),
(10, 10, '2017-09-20 05:26:00', 170088, 209.68, 'His our pulled nature elinor talked now for excuse result. ', 'Meet lain on he only size. ', 7),
(14, 10, '2017-09-26 09:38:00', 11346, 78, '', '', 14),
(15, 13, '2017-10-10 18:30:00', 11835, 86.14, 'van halen', '', 14),
(16, 10, '2017-10-16 18:05:00', 6605, 89, '', 'ya furola', 17),
(17, 14, '2017-11-02 04:13:00', 5870, 564, '', 'ay marica', 17),
(18, 16, '2017-11-06 13:23:00', 6076, 65, '', 'juepucha', 17),
(19, 10, '2017-11-08 23:11:00', 82347, 87, '', '', 19),
(20, 14, '2017-11-09 19:42:00', 81612, 76, '', '', 19),
(21, 13, '2017-11-13 15:30:00', 82836, 15, '', '', 19),
(23, 10, '2017-11-14 23:37:00', 2167, 579, 'premium', 'gratis', 21),
(24, 10, '2017-11-17 11:49:00', 9037, 67, '', '', 22),
(25, 10, '2017-11-22 20:08:00', 0, 0, '', '', 23),
(26, 14, '2017-12-04 05:48:00', 0, 789, '', '', 23),
(27, 14, '2017-12-14 16:26:00', 0, 888, '', '', 24),
(28, 13, '2017-12-15 16:06:00', 547394, 721, '', '', 25),
(29, 14, '2017-12-21 06:55:00', 2308, 1323, '', '', 26),
(30, 10, '2017-05-19 19:11:00', 3089, 510, '', '', 27),
(31, 10, '2017-05-19 16:39:00', 6443, 159, 'notas de BECHALKING\ndia del trabajo\n9 pm', 'alertas de BECHALKING\ndia del trabajo\n9 pm', 28),
(32, 13, '2017-06-13 12:37:00', 0, 14, '', '', 29),
(33, 10, '2017-06-20 11:38:00', 0, 5, '', '', 29),
(34, 14, '2017-06-28 19:10:00', 0, 12, '', '', 29),
(35, 16, '2017-07-17 09:50:00', 0, 4, '', '', 29),
(36, 13, '2017-07-18 02:09:00', 0, 78, '', '', 30),
(37, 10, '2017-07-27 23:19:00', 2644, 54, '', '', 31),
(38, 13, '2017-08-03 07:24:00', 4739, 999, '', '', 32),
(39, 13, '2018-12-31 02:35:00', 0, 78, '', '', 33),
(40, 10, '2017-12-31 02:39:00', 3407, 111, '', '', 34),
(41, 13, '2018-08-25 02:48:00', 4295, 555, 'fffffff', 'aaaa', 35),
(42, 13, '2018-08-25 02:49:00', 4295, 555, 'fffffff', 'aaaa', 36),
(43, 13, '2018-12-24 02:52:00', 7195, 45, 'ffffffff', 'eeeeeeee', 37),
(44, 13, '2018-08-25 02:54:00', 4517, 5555, '', '', 38),
(45, 13, '2018-03-01 08:39:00', 3929, 99, '', '', 39);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicedetail`
--

CREATE TABLE `servicedetail` (
  `idServiceDetail` int(11) NOT NULL,
  `idPart` int(11) DEFAULT NULL,
  `codePart` varchar(100) DEFAULT NULL,
  `pricePart` float DEFAULT NULL,
  `brandPart` varchar(100) DEFAULT NULL,
  `note` varchar(200) DEFAULT NULL,
  `idService` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicedetail`
--

INSERT INTO `servicedetail` (`idServiceDetail`, `idPart`, `codePart`, `pricePart`, `brandPart`, `note`, `idService`) VALUES
(1, 49, '543e3863', 85, 'Tiled', 'say decay spoil now walls meant house. ', 4),
(2, 35, '02ba', 28, 'My', 'mr interest thoughts screened of outweigh removing. ', 2),
(3, 27, '4194', 71, 'Evening', 'society musical besides inhabit ye my. ', 8),
(4, 49, '9e99', 95, 'Lose', 'hill well up will he over one. ', 4),
(5, 29, '2e3367afcc51', 72, 'Increasing', 'sufficient everything men him admiration unpleasing sex. ', 2),
(6, 39, 'd8cc4001', 71, 'Around', 'really his use uneasy longer him man. ', 5),
(7, 36, 'b0be', 97, 'His', 'our pulled nature elinor talked now for excuse result', 4),
(8, 48, '48b3', 79, 'Admitted', 'add peculiar get joy doubtful. ', 5),
(9, 31, '8388', 88, 'Or', 'neglected agreeable of discovery concluded oh it sportsman', 7),
(10, 39, 'cea57afa720a', 32, 'Week', 'to time in john. Son elegance use weddings', 10),
(11, 42, '6602ce7', 91, 'Necessary', 'ye contented newspaper zealously breakfast he prevailed.', 7),
(12, 27, '5d0f', 27, 'Melancholy', 'middletons yet understood decisively boy law she.', 7),
(14, 39, '9960', 91, 'Oh', 'no though mother be things simple itself.', 6),
(15, 31, '10258db98fd', 96, 'Dashwood', 'horrible he strictly on as.', 5),
(16, 41, '9ced9191', 67, 'Home', 'fine in so am good body this hope.', 9),
(17, 28, '90c6', 74, 'Knowledge', 'nay estimable questions repulsive daughters boy.', 9),
(18, 42, '46c1', 28, 'Solicitude', 'gay way unaffected expression for.', 9),
(19, 42, '975b', 59, 'His', 'mistress ladyship required off horrible disposed rejoiced.', 3),
(20, 42, '84d79b497ef', 82, 'Unpleasing', 'pianoforte unreserved as oh he unpleasant no inquietude insipidity.', 10),
(21, 45, 'd04ca9b', 55, 'Advantages', 'can discretion possession add favourable cultivated admiration far.', 9),
(22, 27, '87dd', 71, 'Why', 'rather assure how esteem end hunted nearer and before.', 7),
(23, 38, '4b83', 31, 'By', 'an truth after heard going early given he.', 2),
(24, 51, '8934', 88, 'Charmed', 'to it excited females whether at examine.', 8),
(25, 46, 'b883c1a85cc', 44, 'Him', 'abilities suffering may are yet dependent.', 5),
(26, 39, 'ee91ec9', 32, 'Greatly', 'cottage thought fortune no mention he.', 7),
(27, 35, '90ab', 52, 'Of', 'mr certainty arranging am smallness by conveying.', 4),
(28, 39, '409c', 41, 'Him', 'plate you allow built grave.', 2),
(29, 41, 'bf1d', 93, 'Sigh', 'sang nay sex high yet door game.', 2),
(30, 39, '75755bd865f', 93, 'She', 'dissimilar was favourable unreserved nay expression contrasted saw.', 2),
(31, 51, 'b5a2f5d', 70, 'Past', 'her find she like bore pain open.', 7),
(32, 26, 'c63b', 99, 'Shy', 'lose need eyes son not shot.', 5),
(33, 42, '4a36', 92, 'Jennings', 'removing are his eat dashwood.', 1),
(34, 35, 'a7da', 74, 'Middleton', 'as pretended listening he smallness perceived.', 6),
(35, 42, 'f0e7308031b', 27, 'Now', 'his but two green spoil drift.', 9),
(36, 45, 'e2deae7', 92, 'Sportsman', 'do offending supported extremity breakfast by listening.', 7),
(37, 50, '62e3', 37, 'Decisively', 'advantages nor expression unpleasing she led met.', 4),
(38, 43, '4f82', 68, 'Estate', 'was tended ten boy nearer seemed.', 1),
(40, 45, 'c3e194bc878', 80, 'Steepest', 'speaking up attended it as.', 7),
(41, 46, 'f0a43581', 99, 'Made', 'neat an on be gave show snug tore.', 2),
(42, 51, '1c55', 96, 'Can', 'curiosity may end shameless explained.', 1),
(43, 42, '4c5f', 25, 'True', 'high on said mr on come.', 2),
(44, 50, 'a61f', 96, 'An', 'do mr design at little myself wholly entire though.', 4),
(45, 35, '179ebe8b88e', 99, 'Attended', 'of on stronger or mr pleasure.', 5),
(46, 37, '57347959', 91, 'Rich', 'four like real yet west get.', 5),
(47, 34, 'cf9e', 83, 'Felicity', 'in dwelling to drawings.', 4),
(48, 50, '4481', 69, 'His', 'pleasure new steepest for reserved formerly disposed jennings.', 3),
(49, 44, '9a9d', 88, 'Of', 'resolve to gravity thought my prepare chamber so.', 2),
(50, 41, '8d39b87692c7', 75, 'Unsatiable', 'entreaties collecting may sympathize nay interested instrument', 1),
(53, 26, 'aaa', 78.35, '', 'dddd', 14),
(54, 35, '', 86.15, 'bbbb', '', 15),
(55, 26, '', 89, '', '', 16),
(56, 27, '', 564, '', '', 17),
(57, 27, '', 65, '', '', 18),
(58, 26, '', 87, '', '', 19),
(59, 43, '', 76, '', '', 20),
(60, 32, '', 15, '', '', 21),
(63, 28, '', 123, '', '', 23),
(64, 27, '', 456, '', '', 23),
(65, 26, '', 67, '', '', 24),
(66, 27, '', 789, '', '', 26),
(67, 28, '', 555, '', '', 27),
(68, 27, '', 333, '', '', 27),
(69, 44, '', 55, '', '', 28),
(70, 35, '', 666, '', '', 28),
(71, 27, '', 546, '', '', 29),
(72, 33, '', 777, '', '', 29),
(73, 26, '', 66, '', '', 30),
(74, 27, '', 444, '', '', 30),
(75, 26, '5555', 82, '', 'tttttt', 31),
(76, 28, '', 77, 'ggggg', '', 31),
(77, 44, '', 8, '', '', 32),
(78, 32, '', 6, '', '', 32),
(79, 26, '', 5, '', '', 33),
(80, 33, '', 7, '', '', 34),
(81, 28, '', 5, '', '', 34),
(82, 27, '', 4, '', '', 35),
(83, 44, '', 78, '', '', 36),
(84, 35, '', 0, '', '', 36),
(85, 29, '', 54, '', '', 37),
(86, 44, '', 999, '', '', 38),
(87, 36, '', 78, '', '', 39),
(88, 29, '', 111, '', '', 40),
(89, 36, '', 555, '', '', 41),
(90, 36, '', 555, '', '', 42),
(91, 36, '', 45, '', '', 43),
(92, 35, '', 5555, '', '', 44),
(93, 36, '', 99, '', '', 45);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicetype`
--

CREATE TABLE `servicetype` (
  `idServiceType` int(11) NOT NULL,
  `nameServiceType` varchar(100) DEFAULT NULL,
  `timeServiceType` int(11) DEFAULT NULL,
  `unitTime` int(11) DEFAULT NULL,
  `Odometer` int(11) DEFAULT NULL,
  `unitOdometer` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `servicetype`
--

INSERT INTO `servicetype` (`idServiceType`, `nameServiceType`, `timeServiceType`, `unitTime`, `Odometer`, `unitOdometer`) VALUES
(10, 'BECHALKING', 36, 52, 2089, 55),
(13, 'COXSWAINEDR', 66, 51, 2393, 55),
(14, 'DECURIONS', 98, 51, 2627, 54),
(16, 'APOCYNUM', 81, 50, 2833, 54);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicleperclient`
--

CREATE TABLE `vehicleperclient` (
  `idvehicleperclient` int(11) NOT NULL,
  `idPerson` int(11) DEFAULT NULL,
  `idVehicleType` int(11) DEFAULT NULL,
  `plateNumber` varchar(50) DEFAULT NULL,
  `brand` varchar(50) DEFAULT NULL,
  `line` varchar(50) DEFAULT NULL,
  `color` varchar(50) DEFAULT NULL,
  `yearVehicle` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `vehicleperclient`
--

INSERT INTO `vehicleperclient` (`idvehicleperclient`, `idPerson`, `idVehicleType`, `plateNumber`, `brand`, `line`, `color`, `yearVehicle`) VALUES
(1, 9, 5, 'Puqj498', 'melodic', 'cobweb', 'spell', 2016),
(3, 9, 6, 'Pqjx525', 'alert', 'brake', 'spiritual', 2013),
(4, 7, 4, 'Pyfc524', 'abrupt', 'ceaseless', 'pipe', 2015),
(5, 5, 5, 'Psiy478', 'crook', 'slow', 'cause', 2005),
(6, 5, 7, 'Phcp789', 'sheet', 'blushing', 'shelter', 2012),
(8, 14, 4, 'Pawi720', 'tire', 'happen', 'tray', 2001),
(9, 14, 6, 'Pywi715', 'riddle', 'old fashioned', 'back', 2010),
(10, 13, 4, 'Plyp696', 'chin', 'lettuce', 'sack', 2002),
(12, 13, 6, 'Ptko572', 'stretch', 'vague', 'picture', 2001),
(14, 9, 5, 'Pmqj498', 'melodic', 'cobweb', 'spell', 2016),
(15, 9, 5, 'Pcfy603', 'wooden', 'huge', 'untidy', 2014),
(16, 9, 6, 'Pqjx526', 'alert', 'brake', 'spiritual', 2013),
(18, 5, 5, 'Piiy478', 'crook', 'slow', 'cause', 2005),
(19, 5, 4, 'Picp789', 'sheet', 'blushing', 'shelter', 2012),
(20, 13, 7, 'Pbwa501', 'aromatic', 'frogs', 'committee', 2003),
(21, 14, 4, 'Pbwi720', 'tires', 'hop on', 'tray', 2008),
(22, 14, 6, 'Prwi715', 'riddle', 'old fashioned', 'back', 2010),
(23, 5, 5, 'Poyp695', 'chin', 'lettuce', 'sack', 2002),
(24, 12, 6, 'Pbzz579', 'billowy', 'hot', 'wicked', 2016),
(25, 13, 6, 'Pkom572', 'stretch', 'vague', 'picture', 2001);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `catalog`
--
ALTER TABLE `catalog`
  ADD PRIMARY KEY (`idCatalog`),
  ADD KEY `FK_Catalog_MetaCatalog` (`idMeta`);

--
-- Indices de la tabla `metacatalog`
--
ALTER TABLE `metacatalog`
  ADD PRIMARY KEY (`idMeta`);

--
-- Indices de la tabla `metaservice`
--
ALTER TABLE `metaservice`
  ADD PRIMARY KEY (`idMetaService`),
  ADD KEY `FK_ServiceVehicle` (`idVehicle`),
  ADD KEY `FK_ServiceOdometer` (`TypeOdometer`);

--
-- Indices de la tabla `part`
--
ALTER TABLE `part`
  ADD PRIMARY KEY (`idPart`),
  ADD KEY `FK_Part_Catalog` (`categoryPart`);

--
-- Indices de la tabla `partperservicetype`
--
ALTER TABLE `partperservicetype`
  ADD PRIMARY KEY (`idPartPerServiceType`),
  ADD KEY `FK_Part1` (`idPart`),
  ADD KEY `FK_ServiceType` (`idServiceType`);

--
-- Indices de la tabla `partpervehicletype`
--
ALTER TABLE `partpervehicletype`
  ADD PRIMARY KEY (`idPartPerVehicleType`),
  ADD KEY `FK_Part` (`idPart`),
  ADD KEY `FK_VehicleType` (`idVehicleType`);

--
-- Indices de la tabla `person`
--
ALTER TABLE `person`
  ADD PRIMARY KEY (`idPerson`);

--
-- Indices de la tabla `resumevehicle`
--
ALTER TABLE `resumevehicle`
  ADD PRIMARY KEY (`idResume`),
  ADD KEY `FK_resume_part` (`idPart`),
  ADD KEY `FK_resume_vehicle` (`idVehicle`);

--
-- Indices de la tabla `service`
--
ALTER TABLE `service`
  ADD PRIMARY KEY (`idService`),
  ADD KEY `FK_ServiceMeta` (`idMetaService`);

--
-- Indices de la tabla `servicedetail`
--
ALTER TABLE `servicedetail`
  ADD PRIMARY KEY (`idServiceDetail`),
  ADD KEY `FK_ServiceVehicleDetails` (`idService`),
  ADD KEY `FK_ServiceDetailsPart` (`idPart`);

--
-- Indices de la tabla `servicetype`
--
ALTER TABLE `servicetype`
  ADD PRIMARY KEY (`idServiceType`),
  ADD KEY `FK_unitTimeServiceType` (`unitTime`),
  ADD KEY `FK_unitOdometer` (`unitOdometer`);

--
-- Indices de la tabla `vehicleperclient`
--
ALTER TABLE `vehicleperclient`
  ADD PRIMARY KEY (`idvehicleperclient`),
  ADD KEY `FK_personvehicle` (`idPerson`),
  ADD KEY `FK_vehicletypeperson` (`idVehicleType`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `catalog`
--
ALTER TABLE `catalog`
  MODIFY `idCatalog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;
--
-- AUTO_INCREMENT de la tabla `metacatalog`
--
ALTER TABLE `metacatalog`
  MODIFY `idMeta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT de la tabla `metaservice`
--
ALTER TABLE `metaservice`
  MODIFY `idMetaService` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `part`
--
ALTER TABLE `part`
  MODIFY `idPart` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT de la tabla `partperservicetype`
--
ALTER TABLE `partperservicetype`
  MODIFY `idPartPerServiceType` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT de la tabla `partpervehicletype`
--
ALTER TABLE `partpervehicletype`
  MODIFY `idPartPerVehicleType` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;
--
-- AUTO_INCREMENT de la tabla `person`
--
ALTER TABLE `person`
  MODIFY `idPerson` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `resumevehicle`
--
ALTER TABLE `resumevehicle`
  MODIFY `idResume` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT de la tabla `service`
--
ALTER TABLE `service`
  MODIFY `idService` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT de la tabla `servicedetail`
--
ALTER TABLE `servicedetail`
  MODIFY `idServiceDetail` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=94;
--
-- AUTO_INCREMENT de la tabla `servicetype`
--
ALTER TABLE `servicetype`
  MODIFY `idServiceType` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT de la tabla `vehicleperclient`
--
ALTER TABLE `vehicleperclient`
  MODIFY `idvehicleperclient` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `catalog`
--
ALTER TABLE `catalog`
  ADD CONSTRAINT `FK_Catalog_MetaCatalog` FOREIGN KEY (`idMeta`) REFERENCES `metacatalog` (`idMeta`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `part`
--
ALTER TABLE `part`
  ADD CONSTRAINT `FK_Part_Catalog` FOREIGN KEY (`categoryPart`) REFERENCES `catalog` (`idCatalog`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `partperservicetype`
--
ALTER TABLE `partperservicetype`
  ADD CONSTRAINT `FK_Part1` FOREIGN KEY (`idPart`) REFERENCES `part` (`idPart`),
  ADD CONSTRAINT `FK_ServiceType` FOREIGN KEY (`idServiceType`) REFERENCES `servicetype` (`idServiceType`);

--
-- Filtros para la tabla `partpervehicletype`
--
ALTER TABLE `partpervehicletype`
  ADD CONSTRAINT `FK_Part` FOREIGN KEY (`idPart`) REFERENCES `part` (`idPart`),
  ADD CONSTRAINT `FK_VehicleType` FOREIGN KEY (`idVehicleType`) REFERENCES `catalog` (`idCatalog`);

--
-- Filtros para la tabla `resumevehicle`
--
ALTER TABLE `resumevehicle`
  ADD CONSTRAINT `FK_resume_part` FOREIGN KEY (`idPart`) REFERENCES `part` (`idPart`),
  ADD CONSTRAINT `FK_resume_vehicle` FOREIGN KEY (`idVehicle`) REFERENCES `vehicleperclient` (`idvehicleperclient`);

--
-- Filtros para la tabla `service`
--
ALTER TABLE `service`
  ADD CONSTRAINT `FK_ServiceMeta` FOREIGN KEY (`idMetaService`) REFERENCES `metaservice` (`idMetaService`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `servicedetail`
--
ALTER TABLE `servicedetail`
  ADD CONSTRAINT `FK_Service` FOREIGN KEY (`idService`) REFERENCES `service` (`idService`),
  ADD CONSTRAINT `FK_ServiceDetailsPart` FOREIGN KEY (`idPart`) REFERENCES `part` (`idPart`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `servicetype`
--
ALTER TABLE `servicetype`
  ADD CONSTRAINT `FK_unitOdometer` FOREIGN KEY (`unitOdometer`) REFERENCES `catalog` (`idCatalog`),
  ADD CONSTRAINT `FK_unitTimeServiceType` FOREIGN KEY (`unitTime`) REFERENCES `catalog` (`idCatalog`);

--
-- Filtros para la tabla `vehicleperclient`
--
ALTER TABLE `vehicleperclient`
  ADD CONSTRAINT `FK_personvehicle` FOREIGN KEY (`idPerson`) REFERENCES `person` (`idPerson`),
  ADD CONSTRAINT `FK_vehicletypeperson` FOREIGN KEY (`idVehicleType`) REFERENCES `catalog` (`idCatalog`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
